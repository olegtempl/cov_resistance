import catalogButtonEmodji from "./buttons/catalog"
import settingsButtonEmodji from "./buttons/settings"
import feedbackButtonEmodji from "./buttons/feedback"
import gameButtonEmodji from "./buttons/game"
import languageSetButtonEmodji from "./buttons/languageSet"
import adminButtonEmodji from "./buttons/admin"

export default {
    adminButtonEmodji,
    catalogButtonEmodji,
    settingsButtonEmodji,
    feedbackButtonEmodji,
    gameButtonEmodji,
    languageSetButtonEmodji
}