export default {
    fm: "🏷️",
    liners: {
        header: "🛳",
    },
    countries: {
        header: "📊",
    },

    // chronology: {
    //     header: "Хронология",
    // },
    eduMaterials: {
        header: "🔭",
    },
    document: {
        header: "📖",
    }
}