export default {
  header: "🎲",
  gameMode: {
    easy: {
      header: "👱",
    },
    hard: {
      header: "👨‍🎓",
    },
  }
}