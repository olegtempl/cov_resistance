import buttonsEmodji from "./emodji/buttons"
import textEmodji from "./emodji/text"
import emodjiForNumbers from "./emodji/numbers"


// First 3 letters on word
const {
    gameButtonEmodji: gam,
    catalogButtonEmodji: cat,
    settingsButtonEmodji: set,
    feedbackButtonEmodji: fee,
    adminButtonEmodji: admBtn,
    languageSetButtonEmodji: langSet
} = buttonsEmodji

const {
    addResumeTextEmodji: addResumeText,
    adminTextEmodji: admText,
    adminTxt,
    other,
    events
} = textEmodji;



export default {
    emodjiForButtons: {
        // On Buttons
        nextBtn: "➡️",
        backBtn: "⬅️",
        goMainMenuBtn: "🏠",
        skipBtn: "🏄",
        sendContact: "📟",
        start: "startAddResume",
        yesBtn: "✅",
        noBtn: "❌",
        active: "🔘",
        passive: "⚪",
        up: "🔺",
        down: "🔻",
        link: "🐾",
        admin: "🔐",
        paymentCard: "💳",
        cart: "🛍️",
        presentation: {
            nextBtn: '⏩',
            backBtn: '⏪',
            outBtn: '⏮',
            skipBtn: '⏭',
        },
        admBtn,
        langSet,
        gam,
        cat,
        fee,
        set,
    },
    emodjiForText: {
        // On Text
        addResumeText,
        admText,
        events,
        langSet: {
            hiFavoriteUser: "/../",
            header: ''
        },
        adminTxt,
        mainMenu: "/../",
        other,
        info: "ℹ️",
        congralution: "🥳",
        warning: "🔵"

    },
    emodjiForNumbers
}