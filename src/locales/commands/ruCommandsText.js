import settings from './ruCommandsText/settings'
import game from './ruCommandsText/game'
import feedback from './ruCommandsText/feedback'
import catalog from './ruCommandsText/catalog'
import admin from './ruCommandsText/admin'

export default {
    start: "Про выбор языка в 3 экземплярах",
    mainMenu: {
        header: "Вы находитесь в главном меню бота. Что вас интересует?"
    },
    admin,
    feedback,
    settings,
    game,
    catalog,
    startAddResume: "Начать",
    addPhotoInResume: "Добавить фото",
    yesBtn: "Да",
    noBtn: "Нет",
    skipeBtn: "Пропустить",
    sendContact: "Отправить контакт",
    officialSiteBtn: "переход на сайт",
    commandsList: {
        langSet: {
            ru: "/langru",
            be: "/langbe",
            en: "/langen",
            uk: "/languk",
            pl: "/langpl",
        }
    },
    langSet: {
        header: 'Выберите предпочитаемый язык',
        hiFavoriteUser: "Здарова",
        ru: `👋 Выбран Русский язык`,
        be: `👋 Абрана Беларуская мова`,
    }
}