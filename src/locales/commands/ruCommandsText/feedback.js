export default {
    header: "Ты находишься в меню в котором предоставлены все возможные способы для обратной связи.",
    faq: {
        // Тут будет список вопросов

        header: "Ответы на часто задаваемые вопросы. Выбери каттегорию, в ней вероятно найдется ответ.",
        questionList: "Список",
        answerQuestion: "Вопрос/Ответ",
        questionNumber: "Вопрос №",
        turqoiseOrgs: "Бирюзовые орг-ции",
        aboutDarkDev: {
            header: "О DarkDev",
            orders: "Заказы",
            events: "В этом меню я помогу тебе найти ответы и вопросы на тему мероприятий организуемых проектом.",
            partners: "Партнеры"
        },
        wargaming: "Варгейминг",
        gamification: "Геймификация"
    },
    needPeople: {
        header: "По какому вопросу вам нужен человек?",
        order: "Заказ",
        talk: "Общение",
        work: "Трудоустройство",
        events: "Мероприятия"
    },
    rate: {
        header: "Обратня связь",
        offer: {
            header: "Предложение",
        },
        review: {
            header: "В команды: Что вы хотите оценить? ", // Отзыв
            service: {
                header: "",
            },
            bot: {
                header: "Бот",
            },
            other: {
                header: "Другое",
            }
        }

    },
    cardDescriptionStart: "Выбрана карточка:",
    enterTextForKudoCard: "Введи текст который необходимо расположить на карточке. Чего нибудь лампового и теплого, эти карточки только для позитивной обратной связи.",
    totallyAwesome: "\"Великолепно.\"",
    congratulations: "\"Поздравляю.\"",
    proud: "\"Горжусь.\"",
    creatJob: "\"Отличная работа.\"",
    veryHappy: "\"Очень счастлив.\"",
    thankYou: "\"Спасибо.\"",
    wellDone: "\"Отлично.\"",
    choseYourLanguage: "Нажми одну из кнопок ниже, для выбора языка на котором будет карточка благодарности.",


}