export default {
    header: `Вы находитесь в библиотеке. Выбери пожалуйста что вас интересует.
    🔭 Беларуский трактар - материалы и ссылки на интересные проекты для само-образования на период само-изоляции
    📖 документ "Временные методические рекомендации "Профилактика, диагностика и лечение новой коронавирусной инфекции (2019-nCoV)". Версия 2 (3 февраля 2020 г.) (утв. Министерством здравоохранения РФ)
    📊 хронология событий по странам
    🛳 хронология событий по лайнерам
    `,

}