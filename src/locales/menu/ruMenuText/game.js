export default {
    header: "Начать игру",

    gameMode: {
        easy: {
            header: "Обычный",
        },
        hard: {
            header: "Абитуриент",
        },
    },

    save: {
        header: "Сохранения"
    },
    instruction: [
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
        `Lorem Ipsum is simply dummy text of the printing and typesetting industry.

        💠 It was popularised in the 1960s with the release
        ♻️ It was popularised in the 1960s with the release
        ☣️ It was popularised in the 1960s with the release
        🛑 It was popularised in the 1960s with the release`,
    ],
    instructionLastMsg: "👏 👏 Инструкция окончена, теперь можно начать повествование 😏",
    days: [
        `1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
        `5 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    ],
    daysLastMsg: `👏 👏 Игра окончена. 👏 👏
    Спасибо что играли, надеемся вам было итересно.

    Обратную связь можно оставить по кнопке ниже. Или команде ...
    `,
    nextDay: "Следующий день",
    previewsDay: "Предыдущий день день",
    abbreviation: "Аббревиатуры",
    save: "Сохранить"
}