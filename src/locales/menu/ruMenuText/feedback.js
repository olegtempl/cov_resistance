export default {
    header: `Обратная
связь`,
    faq: {
        header: "F.A.Q",
        turqoiseOrgs: "Бирюзовые орг-ции",
        aboutDarkDev: {
            header: "О DarkDev",
            orders: "Заказы",
            events: "Мероприятия",
            partners: "Партнеры"
        },
        wargaming: "Варгейминг",
        gamification: "Геймификация"

    },
    needPeople: {
        header: "Помощь людей",
        order: "Заказ",
        talk: "Общение",
        work: "Трудоустройство",
        events: "Мероприятия"
    },
    rate: {
        header: "Писать",
        offer: {
            header: "Предложение",
        },
        review: {
            header: "Отзыв",
        },

        service: {
            header: "Сервис",
        },
        event: {
            header: "Мероприятие",
        },
        bot: {
            header: "Бот",
        },
        other: {
            header: "Другое",
        }
    },
}


// totallyAwesome: "Великолепно",
// congratulations: "Поздравляю",
// proud: "Горжусь",
// creatJob: "Отличная работа",
// veryHappy: "Очень счастлив",
// thankYou: "Спасибо",
// wellDone: "Отлично",
// promo: "Изображения карточек",
// pdfFormat: ".pdf",
// jpgFormat: "в *.jpg.",
// allFormat: "Оба формата",
// doza: "в чат команды doza",
// agileGamesCommunity: "в чат Agile Games Community"