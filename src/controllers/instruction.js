const Scene = require('telegraf/scenes/base')
import Markup from 'telegraf/markup'
import Stage from 'telegraf/stage'
const Extra = require('telegraf/extra')

const {
    enter,
    leave
} = Stage

import textData from '../utils/exportTextData'
const instructionScene = new Scene('instruction')

let step = 0;


const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/TeNUEHa'
    ).then(() => next())
}



instructionScene.enter(sendImg, (ctx) => ctx.reply(
        `Перед переходом в главное меню хочу провести тебе инструктаж из ${textData.menuText.instruction.length - 1} шагов.`, Markup
        .keyboard([
            [
                `${textData.eB.skipBtn} ${textData.menuText.skipBtn}`,
                `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
            ]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)


// mainMenuScene.hears(`${textData.eB.cat.fm} ${textData.cmdText.catalog.header}`, printNextStepOnInstruction, enter('catalogStart'))
instructionScene.hears(`${textData.eB.nextBtn} ${textData.menuText.nextBtn}`, (ctx) => {
    step++;
    if (step == textData.menuText.instruction.length) {
        ctx.reply(`${textData.menuText.instructionLastMsg}`, Markup
            .keyboard([
                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`${textData.menuText.instruction[step]}`, Markup
            .keyboard([
                [
                    `${textData.eB.skipBtn} ${textData.menuText.skipBtn}`,
                    `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

instructionScene.hears(`${textData.eB.skipBtn} ${textData.menuText.skipBtn}`, (ctx) => {
    ctx.reply(`${textData.menuText.instructionLastMsg}`, Markup
        .keyboard([
            `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
        ])
        .oneTime()
        .resize()
        .extra())
})

instructionScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))



export default instructionScene