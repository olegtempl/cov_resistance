const Scene = require('telegraf/scenes/base')

import Markup from 'telegraf/markup'
import textData from '../utils/exportTextData'
import Stage from 'telegraf/stage'
const {
    enter,
    leave
} = Stage

const mainMenuScene = new Scene('mainMenu')

mainMenuScene.enter((ctx) => ctx.reply(
        `${textData.menuText.langSet.firstMenuMsg}

        ${textData.menuText.cmdList}
        `, Markup
        .keyboard([
            [`💠 ${textData.menuText.catalog.header}`, ],
            [`${textData.eB.gam.header} ${textData.menuText.game.header}`],
            // [`${textData.eB.fee.header} ${textData.menuText.feedback.header}`, `${textData.eB.set.header} ${textData.menuText.settings.header}`],
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

mainMenuScene.hears(`💠 ${textData.menuText.catalog.header}`, enter('catalogStart'))
mainMenuScene.hears(`${textData.eB.gam.header} ${textData.menuText.game.header}`, enter('game'))
// mainMenuScene.hears(`${textData.eB.abo.header} ${textData.cmdText.about.header}`, ctx => ctx.reply(textData.cmdText.about.aboutMenu))





export default mainMenuScene