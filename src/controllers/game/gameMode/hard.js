const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const hardModeScene = new Scene('hardMode');

const sendImgGame = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/OvkM6gN'
    ).then(() => next())
}


// `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
hardModeScene.enter(sendImgGame, (ctx) => ctx.reply(
    `Данный режим находится в разработке, доступен обычный режим.`, Markup
    .keyboard([
        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, ],
        [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))


// hardModeScene.hears(`${textData.eB.nextBtn} ${textData.menuText.nextBtn}`, enter('hardModeInstruction'))
hardModeScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('gameMode'))
hardModeScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// hardModeScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default hardModeScene