const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'


import daysUrls from '../daysUrls.js'
let step = 0;
const gameModeEasyStartDaysScene = new Scene('gameModeEasyStartDays');


const sendImgGame = (ctx, next) => {
    ctx.replyWithPhoto(
        daysUrls[step]
    ).then(() => next())
}

const sendImgGameFirstScreen = (ctx, next) => {
    ctx.replyWithPhoto(
        "https://imgur.com/QJN3n9V"
    ).then(() => next())
}

gameModeEasyStartDaysScene.enter(sendImgGameFirstScreen, (ctx) => ctx.reply(
        // console.log('start game')

        // step = ctx.savedEasyScene
        `Начало игры

         🗓 Дата:  ${textData.hronologyData.sars[step].date}
         📡 Новости: ${textData.hronologyData.sars[step].news}
         🌡 Новых случаев заболевания: ${textData.hronologyData.sars[step].newDiseases}
         🛎 Новые пораженные регионы: ${textData.hronologyData.sars[step].newRegions}
         💊 Новые регионы излеченные: ${textData.hronologyData.sars[step].clearRegions}

Выполните следующие действия:
    💠 printing and typesetting industry.
    💠 printing and typesetting industry.`, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.game.previewsDay}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`,
            ],
            [
                `🕹 ${textData.menuText.game.save} `,
                `${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`,

            ],
            [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)



gameModeEasyStartDaysScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`, sendImgGame, (ctx) => {
    step++;

    if (step == textData.hronologyData.sars.length) {
        ctx.reply(`${textData.menuText.game.daysLastMsg}`, Markup
            .keyboard([
                [`${textData.eB.gam.header} ${textData.menuText.game.header}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
                ]
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
        День: ${step}

🗓  Дата:  ${textData.hronologyData.sars[step -1].date}
📡 Новости: ${textData.hronologyData.sars[step -1].news}
🌡Новых случаев заболевания: ${textData.hronologyData.sars[step -1].newDiseases}
🛎Новые пораженные регионы: ${textData.hronologyData.sars[step -1].newRegions}
💊 Новые регионы излеченные: ${textData.hronologyData.sars[step -1].clearRegions}

Выполните следующие действия:
💠 printing and typesetting industry.
💠 printing and typesetting industry.`, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.game.previewsDay}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`,
                ],
                [
                    `🕹 ${textData.menuText.game.save}`,
                    `${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`,

                ],
                [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

gameModeEasyStartDaysScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.game.previewsDay}`, sendImgGame, (ctx) => {
    step--;

    if (step == 0) {
        ctx.reply(`
    День: ${step}

🗓  Дата:  ${textData.hronologyData.sars[step -1].date}
📡 Новости: ${textData.hronologyData.sars[step -1].news}
🌡Новых случаев заболевания: ${textData.hronologyData.sars[step -1].newDiseases}
🛎Новые пораженные регионы: ${textData.hronologyData.sars[step -1].newRegions}
💊 Новые регионы излеченные: ${textData.hronologyData.sars[step -1].clearRegions}

Выполните следующие действия:
💠 printing and typesetting industry.
💠 printing and typesetting industry.`, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`,
                ],
                [
                    `🕹 ${textData.menuText.game.save} `,
                    `${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`,

                ],
                [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
        День: ${step}

🗓  Дата:  ${textData.hronologyData.sars[step -1].date}
📡 Новости: ${textData.hronologyData.sars[step -1].news}
🌡Новых случаев заболевания: ${textData.hronologyData.sars[step -1].newDiseases}
🛎Новые пораженные регионы: ${textData.hronologyData.sars[step -1].newRegions}
💊 Новые регионы излеченные: ${textData.hronologyData.sars[step -1].clearRegions}

Выполните следующие действия:
💠 printing and typesetting industry.
💠 printing and typesetting industry.`, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.game.previewsDay}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`,
                ],
                [
                    `🕹 ${textData.menuText.game.save} `,
                    `${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`,

                ],
                [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

gameModeEasyStartDaysScene.hears(`🕹 ${textData.menuText.game.save}`, sendImgGame, (ctx) => {
    ctx.savedEasyScene = step;
    // ctx.state.savedEasyScene = step;

    // console.log(ctx.savedEasyScene)
    // ctx.savedScene.easy = step;
    ctx.reply(`Прогресс сохранён
        День: ${step}

        🗓  Дата:  ${textData.hronologyData.sars[step -1].date}
        📡 Новости: ${textData.hronologyData.sars[step -1].news}
        🌡Новых случаев заболевания: ${textData.hronologyData.sars[step -1].newDiseases}
        🛎Новые пораженные регионы: ${textData.hronologyData.sars[step -1].newRegions}
        💊 Новые регионы излеченные: ${textData.hronologyData.sars[step -1].clearRegions}

        Выполните следующие действия:
        💠 printing and typesetting industry.
        💠 printing and typesetting industry.`, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.game.previewsDay}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.game.nextDay}`,
            ],
            [
                `🕹 ${textData.menuText.game.save} `,
                `${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`,

            ],
            [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra())
})



// gameModeEasyStartDaysScene.hears(`${textData.eB.nextBtn} ${textData.menuText.game.abbreviation}`, enter('mainMenu'))
// gameModeEasyStartDaysScene.hears(`🕹 ${textData.menuText.game.save}`, enter('mainMenu'))
gameModeEasyStartDaysScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))

export default gameModeEasyStartDaysScene