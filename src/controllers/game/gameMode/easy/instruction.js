const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const easyInstructionScene = new Scene('easyInstruction');

let step = 0,
    stepGame = 0;

const sendImgGameBot = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/tgATSdm'
    ).then(() => next())
}

const gameUrls = [
    'https://imgur.com/5ktfM1H',
    'https://imgur.com/nsDbLUJ',
    'https://imgur.com/RzpOKwS',
    'https://imgur.com/xj6uQbQ',
    'https://imgur.com/9Ow8ZhZ',
    'https://imgur.com/EFBlUP2',
    'https://imgur.com/hfv44c5',
    'https://imgur.com/10SpsXz',
    'https://imgur.com/qRTX9H9',
    'https://imgur.com/Xegd3Hq',
    'https://imgur.com/dluyz7P',
    'https://imgur.com/MEZ8hZy',
    'https://imgur.com/JhqGBus',
    'https://imgur.com/szlMFRj',
    'https://imgur.com/FYAGbYt',
    'https://imgur.com/zQA6v9W',
]

const sendImgGameGame = (ctx, next) => {
    stepGame++;


    ctx.replyWithPhoto(
        gameUrls[stepGame]
    ).then(() => next())
}




easyInstructionScene.enter(sendImgGameBot, (ctx) => ctx.reply(
        `${textData.menuText.game.instruction[step]}`, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                `${textData.eB.presentation.skipBtn} ${textData.menuText.skipBtn}`,
            ],
            [
                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
            ],
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

easyInstructionScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImgGameGame, (ctx) => {
    step++;


    if (step == textData.menuText.game.instruction.length) {
        ctx.reply(`${textData.menuText.game.instructionLastMsg}`, Markup
            .keyboard([
                [
                    `${textData.eB.gam.header} Начать игру`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
                ]
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`${textData.menuText.game.instruction[step]}`, Markup
            .keyboard([
                [
                    // `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.skipBtn} ${textData.menuText.skipBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

easyInstructionScene.hears(`${textData.eB.presentation.skipBtn} ${textData.menuText.skipBtn}`, (ctx) => {
    ctx.reply(`${textData.menuText.game.instructionLastMsg}`, Markup
        .keyboard([
            [
                `${textData.eB.gam.header} ${textData.menuText.game.header}`,
                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})



easyInstructionScene.hears(`${textData.eB.gam.header} Начать игру`, enter('gameModeEasyStartDays'))
easyInstructionScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, enter('gameModeEasy'))
easyInstructionScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))

export default easyInstructionScene