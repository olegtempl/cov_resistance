const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const easyModeScene = new Scene('easyMode');

const sendImgGame = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/6y01Xh8'
    ).then(() => next())
}

easyModeScene.enter(sendImgGame, (ctx) => ctx.reply(
    textData.cmdText.game.gameMode.easy.header, Markup
    .keyboard([
        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`],
        [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))


easyModeScene.hears(`${textData.eB.nextBtn} ${textData.menuText.nextBtn}`, enter('easyInstruction'))
easyModeScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('gameMode'))
easyModeScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// easyModeScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default easyModeScene