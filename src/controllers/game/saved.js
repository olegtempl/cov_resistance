const Scene = require('telegraf/scenes/base')
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const {
    enter,
    leave
} = Stage


const gameSavedScene = new Scene('savedGames');

// if (gameSavedScene.savedEasyScene == '0') {
// gameSavedScene.enter((ctx) => ctx.reply(
//     `У вас нету сохраненных игр`, Markup
//     .keyboard([
//         [
//             `👱 перейти в обычный режим`,
//             `👨‍🎓 перейти в сложный режим`
//         ],
//         [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
//     ])
//     .oneTime()
//     .resize()
//     .extra()
// ))
// } else {
gameSavedScene.enter((ctx) => ctx.reply(
    `Выберите режим для перехода в сохраненный прогресс, `, Markup
    .keyboard([
        [
            `👱 сохранение в обычном режиме`,
            `👨‍🎓 сохранение в сложном режиме`
        ],
        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))
// }

gameSavedScene.hears(`👱 сохранение в обычном режиме`, enter('gameModeEasyStartDays'))
// gameSavedScene.hears(`👨‍🎓 перейти к сохранению сложного режима`, enter('gameModeHardStartDays'))

gameSavedScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('game'))
gameSavedScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// gameSavedScene.on('message', (ctx) => ctx.reply(msgText.ifUserSendMsgVicePressBtn))

export default gameSavedScene