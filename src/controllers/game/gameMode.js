const Scene = require('telegraf/scenes/base')
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const {
    enter,
    leave
} = Stage


const gameModeScene = new Scene('gameMode');

const sendImgGame = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/0fBws9B'
    ).then(() => next())
}

gameModeScene.enter(sendImgGame, (ctx) => ctx.reply(
    textData.cmdText.game.gameMode.header, Markup
    .keyboard([

        // [` ${textData.eB.gam.gameMode.easy} ${textData.menuText.game.gameMode.easy}`,
        //    ${textData.eB.gam.gameMode.hard}  `${textData.menuText.game.gameMode.hard}`,
        // ]
        [
            `👱 Обычный`,
            `👨‍🎓 Абитуриент`
        ],
        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))

gameModeScene.hears(`👱 Обычный`, enter('easyMode'))
gameModeScene.hears(`👨‍🎓 Абитуриент`, enter('hardMode'))

gameModeScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('game'))
gameModeScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// gameModeScene.on('message', (ctx) => ctx.reply(msgText.ifUserSendMsgVicePressBtn))

export default gameModeScene