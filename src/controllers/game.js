const Scene = require('telegraf/scenes/base')

const {
        enter,
        leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../utils/exportTextData'

const gameScene = new Scene('game');

const sendImgGame = (ctx, next) => {
        ctx.replyWithPhoto(
                'https://imgur.com/60JbsSb'
        ).then(() => next())
}


gameScene.enter(sendImgGame, (ctx) => ctx.reply(
                textData.cmdText.game.header, Markup
                .keyboard([
                        [
                                `🕹 Сохранения`
                        ],
                        [
                                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                                `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
                        ]
                ])
                .oneTime()
                .resize()
                .extra()
        )

)

gameScene.hears(`🕹 Сохранения`, enter('savedGames'))
gameScene.hears(`${textData.eB.nextBtn} ${textData.menuText.nextBtn}`, enter('gameMode'))
gameScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))

export default gameScene