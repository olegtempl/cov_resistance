const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'


let step = 0;

const germanyScene = new Scene('germany');

const hronologyData = [
    `💠 It was popularised in the 1960s with the release
♻️ It was popularised in the 1960s with the release
`,
    `💠 It was popularised in the 1960s with the release
♻️ It was popularised in the 1960s with the release
☣️ It was popularised in the 1960s with the release
`,
    `💠 It was popularised in the 1960s with the release
♻️ It was popularised in the 1960s with the release
☣️ It was popularised in the 1960s with the release
🛑 It was popularised in the 1960s with the release`
];


const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/dbWTuBP'
    ).then(() => next())
}


germanyScene.enter(sendImg, (ctx) => ctx.reply(
    `Выбрана страна: Германия

💠 It was popularised in the 1960s with the release
`, Markup
    .keyboard([
        [
            `${textData.eB.skipBtn} ${textData.menuText.skipBtn}`,
            `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
        ]
    ])
    .oneTime()
    .resize()
    .extra()
))

germanyScene.hears(`${textData.eB.nextBtn} ${textData.menuText.nextBtn}`, (ctx) => {
    step++;
    if (step == hronologyData.length) {
        ctx.reply(`Все будет хорошо`, Markup
            .keyboard([
                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`${hronologyData[step]}`, Markup
            .keyboard([
                [
                    `${textData.eB.skipBtn} ${textData.menuText.skipBtn}`,
                    `${textData.eB.nextBtn} ${textData.menuText.nextBtn}`
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

germanyScene.hears(`${textData.eB.skipBtn} ${textData.menuText.skipBtn}`, (ctx) => {
    ctx.reply(`Все будет хорошо`, Markup
        .keyboard([
            `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
        ])
        .oneTime()
        .resize()
        .extra())
})


// germanyScene.hears(``, enter(''))

germanyScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('countries'))
germanyScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// germanyScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default germanyScene