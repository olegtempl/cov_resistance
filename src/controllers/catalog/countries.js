const Scene = require('telegraf/scenes/base')
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const {
    enter,
    leave
} = Stage


const countriesScene = new Scene('countries');

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/zQA6v9W'
    ).then(() => next())
}



countriesScene.enter(sendImg, (ctx) => ctx.reply(
    `Хрология развития короновирусев по странам мира.

    Для примера приведена 1 страна 😏`, Markup
    .keyboard([
        [
            '🇩🇪 Германия'
        ],

        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))

countriesScene.hears('🇩🇪 Германия', enter('germany'))





countriesScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('catalogStart'))
countriesScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// countriesScene.on('message', (ctx) => ctx.reply(msgText.ifUserSendMsgVicePressBtn))

export default countriesScene