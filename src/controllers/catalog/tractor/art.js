const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorArtScene = new Scene('tractorArt');
let step = 0;
const artCat = [{
        heading: "ТЕАТР ФРАНКО",
        description: "Архвные записи спектаклей",
        url: "https://www.youtube.com/channel/UC6PLkJPkrioar9d_FUuNf3A/featured",
    },
    {
        heading: "OPEN THEATRE",
        description: "Записи спектаклей вместе с театральным закулисьем + видеолекции Елены Мигашко",
        url: "https://opentheatre.net/",
    },
    {
        heading: "GOOGLE ARTS & CULTURE",
        description: "Прогулки по известнейшим мировым достопримечательностям, музеям, дворцам",
        url: "https://artsandculture.google.com/",
    },
    {
        heading: "СИКСТИНСКАЯ КАПЕЛЛА",
        description: "Виртуальный тур",
        url: "http://www.museivaticani.va/content/museivaticani/en/collezioni/musei/cappella-sistina/tour-virtuale.html",
    },
    {
        heading: "ЛУВР",
        description: "Виртуальные экскурсии по музею",
        url: "https://www.louvre.fr/en/media-en-ligne",
    },
    {
        heading: "МУЗЕЙ ДАЛИ",
        description: "Виртуальный тур",
        url: "https://www.salvador-dali.org/en/museums/dali-theatre-museum-in-figueres/visita-virtual/",
    },
    {
        heading: "МУЗЕЙ ГУГГЕНХАЙМА",
        description: "В онлайн-коллекции представлено около 1700 произведений искусства",
        url: "https://www.guggenheim.org/collection-online",
    },
    {
        heading: "PRADO MUSEUM",
        description: "Знакомство с работами известных художников в онлайн",
        url: "https://www.museodelprado.es/en/the-collection",
    },
    {
        heading: "БРИТАНСКИЙ МУЗЕЙ",
        description: "Виртуальные экскурсии по музею и экспозициям",
        url: "https://www.youtube.com/user/britishmuseum",
    },
    {
        heading: "ДОВЖЕНКО-ЦЕНТР",
        description: `Видеолекции об "украинском" Голливуде`,
        url: "https://www.youtube.com/channel/UCEnrVkHgwvoVlkcQmBKR2_Q",
    },
    {
        heading: "ARZAMAS",
        description: "Проект, посвященный истории культуры. Бесплатный доступ по промокоду КАРАНТИН до 15 апреля",
        url: "https://arzamas.academy/",
    },
    {
        heading: "WIKIART",
        description: "Онлайн-энциклопедия визуальных искусств",
        url: "https://www.wikiart.org/uk",
    },
    {
        heading: "EUROPEANA COLLECTIONS",
        description: "Культурное наследие Европы онлайн",
        url: "https://www.europeana.eu/en",
    },
    {
        heading: "ТРЕТЬЯКОВКА",
        description: "Виртуальные выставки",
        url: "https://www.tretyakovgallery.ru/exhibitions/?type=virtualnye-vystavki&places=museum,new,tg",
    },
    {
        heading: "СИНХРОНИЗАЦИЯ",
        description: "Культурная платформа. Есть бесплатный контент",
        url: "https://synchronize.ru/online",
    },
    {
        heading: "PROJECT GUTENBERG",
        description: "Старейшая универсальная онлайн-библиотека",
        url: "https://www.gutenberg.org/",
    },
    {
        heading: "ЭРМИТАЖ",
        description: "Виртуальная экскурсия, снятая на айфон",
        url: "https://www.youtube.com/watch?v=_MU73rsL9qE",
    },
    {
        heading: "МУЗЕИ УКРАИНЫ",
        description: "Виртуальный тур по музеям под открытым небом",
        url: "https://museums.authenticukraine.com.ua/ua/?fbclid=IwAR0jfjxyyAHqGfLq4ILgSlUfa0YIsPIXoLLvqnIJl8F0gCIDXtqoLPz3mSw",
    },
    {
        heading: "KZ GALLERY",
        description: "Виртуальная галерея современного искусства",
        url: "http://kzgallery.com/ru/",
    },
    {
        heading: "MOBILE THEATRE",
        description: "Мобильный художественный театр открыл доступ к некоторым постановкам",
        url: "https://mobiletheater.io/",
    },
    {
        heading: "АЛЕКСАНДРИНСКИЙ ТЕАТР",
        description: "Спектакли в живой онлайн-трансляции",
        url: "https://alexandrinsky.ru/",
    },
    {
        heading: "ТЕАТР ПРЕКРАСНЫЕ ЦВЕТЫ",
        description: `Онлайн-запись спектакля "ДПЮ"`,
        url: "https://www.youtube.com/watch?v=hi3DYxmz2x0",
    },
    {
        heading: "ДИКИЙ ТЕАТР",
        description: `Видео-версия стектакля "Мы все взрослые люди"`,
        url: "https://www.youtube.com/watch?v=NuiG3NZa9aY",
    },
    {
        heading: "ЛЬВОВСКИЙ АКАДЕМТЕАТР",
        description: "Записи спектаклей",
        url: "https://www.youtube.com/channel/UCPgVrGWJKNmrDmvKOTjanWw/videos",
    },
    {
        heading: "ТЕАТР ЛЕСИ УКРАИНКИ",
        description: "Видео-записи спектаклей",
        url: "https://www.youtube.com/channel/UCmsF1TPSLZXTv_EsW6YU3Og",
    },
    {
        heading: "МУЗЕИ ЛЬВОВЩИНЫ",
        description: "Виртуальный тур",
        url: "https://loda.gov.ua/virtual_museums/",
    },
    {
        heading: "МУЗЕЙ ХАНЕНКО",
        description: "Виртуальный тур",
        url: "https://my.matterport.com/show/?m=GR38x621JeA&fbclid=IwAR1_4CVIf9v_x0xYt0xANttLUZJojuIB-ftL-V0sCc23tNYqk2dulCpcdho",
    },
    {
        heading: "МУЗЕЙ АВИАЦИИ",
        description: "Виртуальный тур",
        url: "http://aviamuseum.com.ua/3d.html",
    },
    {
        heading: "СИЛА ПОДПИСИ",
        description: "Виртуальный тур по выставке",
        url: "https://sk.ua/uk/sila-pidpisu/",
    },
    {
        heading: "UKRAINE WOW",
        description: "Виртуальный тур по выставке",
        url: "https://ukrainewow.com/",
    },
    {
        heading: "МАЙН КАМПФ",
        description: "Или носки в кофейнике. Запись спектакля Театра драмы и комедии на Левом берегу",
        url: "https://www.youtube.com/watch?v=2UIW1q2aNEI&feature=emb_logo",
    },

]

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/1fxlC0O'
    ).then(() => next())
}


tractorArtScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Искусство

🔎 Имя проекта: ${artCat[step].heading}
📝 Описание: ${artCat[step].description}
⛓ Ссылка: ${artCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorArtScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == eduCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${artCat[step].heading}
📝 Описание: ${artCat[step].description}
⛓ Ссылка: ${artCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorArtScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${artCat[step].heading}
📝 Описание: ${artCat[step].description}
⛓ Ссылка: ${artCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorArtScene.hears(``, enter(''))

tractorArtScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorArtScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorArtScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorArtScene