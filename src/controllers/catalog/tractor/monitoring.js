const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorMonitoringScene = new Scene('tractorMonitoring');
let step = 0;

const monCat = [{
    heading: "Тут точно что-то будет",
    description: "И тут тоже появиться",
    url: "Все будет хорошо",
}, ]

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/JvepRf2'
    ).then(() => next())
}


tractorMonitoringScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Мониторинг

🔎 Имя проекта: ${monCat[step].heading}
📝 Описание: ${monCat[step].description}
⛓ Ссылка: ${monCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorMonitoringScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == monCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${monCat[step].heading}
📝 Описание: ${monCat[step].description}
⛓ Ссылка: ${monCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorMonitoringScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${monCat[step].heading}
📝 Описание: ${monCat[step].description}
⛓ Ссылка: ${monCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorMonitoringScene.hears(``, enter(''))

tractorMonitoringScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorMonitoringScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorMonitoringScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorMonitoringScene