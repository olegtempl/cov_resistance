const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorBarsScene = new Scene('tractorBars');
let step = 0;
const barCat = [{
        heading: "STAY THE FUCK HOME",
        description: "Оставайся дома и не пей в одиночку. Попасть практически нереально",
        url: "",
    },
    {
        heading: "ENGLISH, DO YOU SPEAK IT?",
        description: "Как бы для иностранцев, но бухают славяне. Обычно пару мест есть",
        url: "",
    },
    {
        heading: "KRASIVO",
        description: "Дизайн-бар, арт, все дела. Иногда места бывают",
        url: "https://whereby.com/krasivo",
    },
    {
        heading: "BABYLON",
        description: "So. Much. Bar. Самый многолюдный. Пока в бете, но тусы интернациональные",
        url: "https://staythefuckhomebar.whereby.com/babylon",
    },
    {
        heading: "KARTULI",
        description: "Чача, душевные беседы, радушность. Наполняется аудиторией",
        url: ".",
    },
    {
        heading: "ЗАКАТ",
        description: "Пока не очень понятно, но люди ходя",
        url: "https://staythefuckhomebar.whereby.com/zakat",
    },
    {
        heading: "РЮМОЧНАЯ CAMUS",
        description: "Экзистенциализм, граничащий с активностью коммуникации",
        url: "https://staythefuckhomebar.whereby.com/camus",
    },
    {
        heading: "BORODA BAR",
        description: "Вчера и всегда будет борода. В хорошем смысле",
        url: "https://staythefuckhomebar.whereby.com/borodabar",
    },
    {
        heading: "IO RESTO AL BANCO",
        description: "Итальянский бар. Судя по всему, здесь должно быть шумно",
        url: "https://staythefuckhomebar.whereby.com/io-resto-al-banco",
    },
    {
        heading: "I SPEAK YOU LISTEN",
        description: "Бар-лекторий. Интересные рассказы под вино. Или не вино",
        url: "https://staythefuckhomebar.whereby.com/i-speak-you-listen",
    },
    {
        heading: "KUZNYAHOUSE",
        description: "Физически дистанцированно, эмоционально вплотную",
        url: "https://staythefuckhomebar.whereby.com/kuznya",
    },
    {
        heading: "НЕБО И ВИНО",
        description: "Винный питерский бар с аутентичной атмосферой",
        url: "https://staythefuckhomebar.whereby.com/nebo-&-vino",
    },
]

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/bywGuIp'
    ).then(() => next())
}


tractorBarsScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Онлайн-бары

🔎 Имя проекта: ${barCat[step].heading}
📝 Описание: ${barCat[step].description}
⛓ Ссылка: ${barCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorBarsScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == barCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${barCat[step].heading}
📝 Описание: ${barCat[step].description}
⛓ Ссылка: ${barCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorBarsScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${barCat[step].heading}
📝 Описание: ${barCat[step].description}
⛓ Ссылка: ${barCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorBarsScene.hears(``, enter(''))

tractorBarsScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorBarsScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorBarsScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorBarsScene