const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorEducationScene = new Scene('tractorEducation');
let step = 0;
const eduCat = [{
        heading: "COURSERA",
        description: "Бесплатный доступ до 31 июля к 3800+ нурсам и 400+ специализациям",
        url: "https://www.coursera.org/campus/",
    },
    {
        heading: "SKILLSHARE",
        description: "2 месяца бесплатной премиум-подписки и доступ ко всем курсам по фотографии, UI/UX-дизайну, иллюстрации, музыки, кино",
        url: "https://www.skillshare.com/?via=header",
    },
    {
        heading: "АНДРЕЙ ФЕДОРИВ",
        description: `Бесплатный курс "Brand Father"`,
        url: "https://www.youtube.com/watch?v=CsAlVZMfHzE",
    },
    {
        heading: "АНДРЕЙ ШАТЫРКО",
        description: "Бесплатный курс «Продвижение и заработок на YouTube»",
        url: "https://shatyrko.com/forpeople",
    },
    {
        heading: "РОМАН ЕРЕМЕЕВ",
        description: `Бесплатный курс о натуральном вине "Supernatural"`,
        url: "https://openmind.academy/courses/supernatural",
    },
    {
        heading: "КУЛЬТУРНЫЙ ПРОЕКТ",
        description: "Альтернативное образование в сфере искусства. Обучение платное",
        url: "https://culturalproject.org/",
    },
    {
        heading: "GOOGLE DIGITAL WORKSHOP",
        description: "Бесплатные курсы по цифровому маркетингу, рекламе и онлайн-безопасности",
        url: "https://learndigital.withgoogle.com/digitalworkshop-ua/courses",
    },
    {
        heading: "KADENZE",
        description: "Платформа образования в области искусства и креативных технологий",
        url: "https://www.kadenze.com",
    },
    {
        heading: "DATACAMP",
        description: "Платформа образования в IT-сфере",
        url: "https://www.datacamp.com/",
    },
    {
        heading: "EDX",
        description: "Образовательная онлайн-платформа",
        url: "https://www.edx.org/",
    },
    {
        heading: "CLASS CENTRAL",
        description: "Бесплатная образовательная платформа",
        url: "https://www.classcentral.com/",
    },
    {
        heading: "CANVAS",
        description: "Профессиональное образование для педагогов",
        url: "https://www.classcentral.com/",
    },
    {
        heading: "IVERSITY",
        description: "Онлайн-образование в сфере бизнеса и маркетинга",
        url: "https://iversity.org/en",
    },
    {
        heading: "EDUOPEN",
        description: "Бесплатная образовательная платформа",
        url: "https://learn.eduopen.org",
    },
    {
        heading: "OPENLEARN",
        description: "Бесплатная образовательная платформа",
        url: "https://www.open.edu/openlearn/free-courses/full-catalogue",
    },
    {
        heading: "UDEMY",
        description: "Образовательная онлайн-платформа",
        url: "https://www.udemy.com/",
    },
    {
        heading: "STARTUP SCHOOL",
        description: "Бесплатный онлайн курс от Y Combinator",
        url: "https://www.startupschool.org/",
    },
    {
        heading: "PROMETHEUS",
        description: "Платформа массовых открытых онлайн-курсов",
        url: "https://prometheus.org.ua/courses-catalog/",
    },
    {
        heading: "ТИЛЬДОШНАЯ",
        description: "Бесплатный курс по фрилансу для вебдизайнеров",
        url: "https://tildoshnaya.pro/freefreelance",
    },
    {
        heading: "BANGBANG EDUCATION",
        description: "Бесплатные 46 онлайн-курсов Дизайн-библиотеки до 17 апреля",
        url: "https://bangbangeducation.ru/subscription",
    },
    {
        heading: "AWDEE",
        description: "Бесплатный доступ к архиву мега-рассылки «Дизайнерский дайджест» до конца апреля",
        url: "https://awdee.ru/newsletter/",
    },
    {
        heading: "PROJECTOR",
        description: "50% скидки на видеобиблиотеку до 3 апреля 2020",
        url: "https://prjctr.online/library-subscription",
    },
    {
        heading: "TEDхKYIV",
        description: "Свободный доступ к видео выступлениям в рамках конференций TED в Киеве",
        url: "https://tedxkyiv.com/video/",
    },
    {
        heading: "WISECOW",
        description: "Онлайн-лекции по литературе, кино, искусству, музыке, журналистике, театру, истории, моде и социуму",
        url: "https://wisecow.com.ua/all-courses/",
    },
    {
        heading: "УНИВЕРСИТЕТ МАЙДАНА",
        description: "Бесплатные образовательные курсы в сфере гражданского общества",
        url: "https://vumonline.ua/courses/",
    },
    {
        heading: "SETTERS",
        description: "Бесплатные записи с конференции Коллеги",
        url: "https://kollegi.setters.digital/",
    },
    {
        heading: "МАКСИМ ИЛЬЯХОВ",
        description: `Бесплатный аудиокурс "Текст и деньги". Доступен до 30 марта`,
        url: "https://soundcloud.com/slow-down-ru/sets/aj356e98bief/s-oaNkb?fbclid=IwAR1HX4GV9jDGlF3BLdbrOhzjvSrUR7NCOT-sHxQ5KNqBezr_5IFp9C-eLE4",
    },
    {
        heading: "СИНХРОНИЗАЦИЯ",
        description: "История искусства в 10 шедеврах. Бесплатный курс на период карантина",
        url: "https://online.synchronize.ru/10paint",
    },
    {
        heading: "СИНХРОНИЗАЦИЯ",
        description: "История кино в 10 фильмах. Бесплатный курс на период карантина",
        url: "https://online.synchronize.ru/cinema_films",
    },
    {
        heading: "ПОСТНАУКА",
        description: "Курсы проекта о современной фундаментальной науке",
        url: "https://postnauka.ru/courses",
    },
    {
        heading: "БИЗНЕС КЛАСС",
        description: "Бесплатная программа для предпринимателей от Google и Сбербанка",
        url: "https://business-class.pro/ab6?utm_expid=.5KQtsDV3QRuUGSxZkYfy3Q.1&utm_referrer=https:%2F%2Fbusiness-class.pro%2Fab6",
    },
    {
        heading: "NOVIKOV SCHOOL",
        description: "Бесплатный просмотр 300 видеоуроков от лучших шеф-поваров по промокоду SYSOEV",
        url: "https://novikovschool.com/videos/all",
    },
    {
        heading: "FLOMASTER",
        description: "Дискуссионный клуб о жизни и обществе. На бесплатной основе",
        url: "https://flomaster.pro/",
    },
    {
        heading: "PAZHYNA MARAFON",
        description: "Бесплатные эфиры от лучших кондитеров",
        url: "https://www.instagram.com/pazhyna.marafon/",
    },
    {
        heading: "PAZHYNA SCHOOL",
        description: "Бесплатные кондитерские видеоуроки Екатерины Пажиной",
        url: "https://drive.google.com/drive/folders/1RSJiXRJohO44Id-lGwLdZcN96Oaf1mZo",
    },
    {
        heading: "STEPIK",
        description: "Бесплатные курсы по математике, программированию, естественным и гуманитарным наукам",
        url: "https://stepik.org/catalog",
    },
    {
        heading: "DESIGN IGRA",
        description: "Онлайн-курс для дизайнеров. Первая неделя бесплатно",
        url: "https://designigra.com/karantin",
    },
    {
        heading: "SAY HI",
        description: "Много уроков по рисованию, туториалы по графическому дизайну",
        url: "https://say-hi.me/category/risovanie",
    },
    {
        heading: "SOUND GYM",
        description: "Упражнения для тренировки слуха для музыкантов и звукорежиссеров",
        url: "https://www.soundgym.co/site/index",
    },
    {
        heading: "LOOKERY LAB",
        description: "50% скидка на онлайн-курс по росписи тканей в технике холодного батика",
        url: "http://lookerylab.com/kurs_batik_50",
    },
    {
        heading: "BIG MONEY",
        description: "Бесплатный онлайн-марафон о развитии бизнеса в кризис. 10 прямых включений с лидерами индустрий",
        url: "https://bigmoneyconf.com/online-marathon/",
    },
    {
        heading: "МИРОСЛАВА УЛЬЯНИНА",
        description: "Персональный диетолог, нутрициолог. Прямые эфиры и записи с рекомендациями о правильном питании",
        url: "https://www.instagram.com/myroslava_ulianina/",
    },
    {
        heading: "SUPERLUDI",
        description: "Онлайн-обучение в Бизнес-школе новой экономики",
        url: "https://superludi.com/",
    },
    {
        heading: "СВЕТ",
        description: "Видео-лекции о деятельности и личной эффективности",
        url: "https://svet.education/ru/lections",
    },
    {
        heading: "LABA",
        description: "Образовательная платформа о бизнесе, маркетинге, менеджменте",
        url: "https://l-a-b-a.com/lecture",
    },
    {
        heading: "UDACITY",
        description: "Большая международная образовательная платформа",
        url: "https://www.udacity.com/",
    },
    {
        heading: "СБЕРБАНК",
        description: "Управление дистанционными командами и сотрудниками. Электронный курс",
        url: "https://course.sberbank-school.ru/",
    },
    {
        heading: "БАГАЖ",
        description: "Курсы школы маркетинга и коммуникаций. Бесплатный доступ до 19 апреля",
        url: "https://baggage.school/online/",
    },
    {
        heading: "K10STREET",
        description: "Онлайн-занятия по акварели и линогравюре. Ссылка на регистрацию в профиле инстаграм",
        url: "https://www.instagram.com/k10street/",
    },
    {
        heading: "HEADSPACE",
        description: "Ресурс об обучении медитации. Есть бесплатные курсы",
        url: "https://www.headspace.com/",
    },
    {
        heading: "SPIZH STUDIO",
        description: "Канал Жени Спижового о каллиграфии, леттеринге. На время карантина выложен бесплатный курс",
        url: "https://t.me/spizhstudio",
    },
    {
        heading: "UNIVERCITY",
        description: "Курсы удаленных инста-профессий от Университета digital-профессий",
        url: "http://universitybu.top/freekarantin",
    },
    {
        heading: "НЕТОЛОГИЯ",
        description: "Площадка для профессионального роста открыла бесплатный доступ к библиотеке миникурсов по промокоду STAYHOME",
        url: "https://l.netology.ru/stayhome",
    },
    {
        heading: "DEV REPUBLIC",
        description: "В пространстве для карьерного роста открытые онлайн-курсы",
        url: "https://devrepublik.com/ru/kiev-courses/",
    },
    {
        heading: "TOP UNI",
        description: "Телеграм-канал об образовании в UK. Есть архив лекций",
        url: "https://t.me/joinchat/AAAAAFhaoyOiuPLuCKpepQ",
    },
    {
        heading: "ОТКРЫТОЕ ОБРАЗОВАНИЕ",
        description: "500+ онлайн-курсов по базовым дисциплинам российских университетов",
        url: "https://openedu.ru/course/",
    },
    {
        heading: "ФИНАРИУМ",
        description: "Онлайн-платформа для обучения инвестициям и финансовой грамотности",
        url: "https://finarium.pro/",
    },
    {
        heading: "ЭРИК ШЕВЧЕНКО",
        description: "Руководство по верстке текста",
        url: "https://erikshevchenko.ru/potrfolio/verstka/",
    },
    {
        heading: "ЛЕКТОРИУМ",
        description: "Образовательный проект. 5000+ видеолекций, 100+ онлайн-курсов для дистанционного обучения",
        url: "https://www.lektorium.tv/",
    },
    {
        heading: "ORORO",
        description: "Изучение иностранных языков с помощью видео с субтитрами, которые переводятся на лету",
        url: "https://ororo.tv/ru",
    },
    {
        heading: "CASES",
        description: "Архив лекций о дизайне, маркетинге, PR",
        url: "https://cases.media/videos",
    },
    {
        heading: "PROFI SPACE",
        description: "Онлайн-курсы, лекции бизнес-школы",
        url: "https://www.facebook.com/pg/profispaceschool/events/",
    },
    {
        heading: "ITVDN",
        description: "Видео-курсы по программированию. Для школьников и студентов бесплатно до 30 апреля",
        url: "https://itvdn.com/ru/shares/free-courses2020",
    },
    {
        heading: "ОМБИЗ КАРТЕЛЬ",
        description: "Доступ к универсальной подборке полезных материалов о вебинарах",
        url: "https://openmind.com.ua/webinars/ombizweb/",
    },
    {
        heading: "HAPPY MONDAY",
        description: "Карьерный портал и платформа профессионального развития с вакансиями, статьями, возможностями (укр)",
        url: "https://happymonday.ua/",
    },
    {
        heading: "С МЕСТА В КАРЬЕРУ",
        description: "Бесплатный курс о карьерных изменениях от EdEra и Фонда Елены Пинчук (укр)",
        url: "https://ican.ed-era.com/",
    },
    {
        heading: "SKILLS LAB",
        description: "Курс о развитии карьеры, проектном менеджменте и навыках презентации (укр)",
        url: "https://impactorium.org/uk/courses/skills-lab-successful-career/#learndash-course-content",
    },
];

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/Up4cwS1'
    ).then(() => next())
}


tractorEducationScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Образование

🔎 Имя проекта: ${eduCat[step].heading}
📝 Описание: ${eduCat[step].description}
⛓ Ссылка: ${eduCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorEducationScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == eduCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${eduCat[step].heading}
📝 Описание: ${eduCat[step].description}
⛓ Ссылка: ${eduCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorEducationScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${eduCat[step].heading}
📝 Описание: ${eduCat[step].description}
⛓ Ссылка: ${eduCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorEducationScene.hears(``, enter(''))

tractorEducationScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorEducationScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorEducationScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorEducationScene