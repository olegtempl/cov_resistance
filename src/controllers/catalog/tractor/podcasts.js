const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorPodcastsScene = new Scene('tractorPodcasts');
let step = 0;
const podCat = [{
        heading: "ВЕНСКАЯ ОПЕРА",
        description: "На время пандемии открыты и планируются ежедневные трансляции балетов и оперы",
        url: "https://www.staatsoperlive.com/",
    },
    {
        heading: "БАВАРСКАЯ ОПЕРА",
        description: "Шесть прямых трансляций сезона 2019/2020",
        url: "https://operlive.de/",
    },
    {
        heading: "БЕРЛИНСКАЯ ОПЕРА",
        description: "Онлайн-трансляции спектаклей",
        url: "https://www.staatsoper-berlin.de/de/",
    },
    {
        heading: "ДАТСКАЯ ОПЕРА",
        description: "Прямые трансляции и записи",
        url: "https://www.youtube.com/user/DutchNatOperaBallet/videos",
    },
    {
        heading: "МЕТРОПОЛИТЕН ОПЕРА",
        description: "Видео спектаклей, записанных на протяжении последних 14 лет",
        url: "https://www.metopera.org/",
    },
    {
        heading: "DIGITAL CONCERT HALL",
        description: "Бесплатный доступ к проекту Берлинской Филармонии по промокоду BERLINPHIL. Активировать нужно до 31 марта",
        url: "https://www.digitalconcerthall.com/en/home",
    },
    {
        heading: "СПб ФИЛАРМОНИЯ",
        description: "Бесплатные онлайн-трансляции",
        url: "https://www.philharmonia.spb.ru/media/online/",
    },
    {
        heading: "LA MONNAIE",
        description: "Виртуальный сезон Брюссельского оперного театра. Бесплатный доступ до 19 апреля",
        url: "https://www.lamonnaie.be/en/sections/388-mm-channel",
    },
    {
        heading: "NOTALONE",
        description: "DJ-стримы со всего мира",
        url: "https://notalone.live/",
    },
    {
        heading: "UNITED WE STREAM",
        description: "Самый большой цифровой клуб в мире. Для просмотра живых выступлений требуется пожертвование в любом размере",
        url: "https://en.unitedwestream.berlin/",
    },
    {
        heading: "CLOSER",
        description: "Стримы клуба вживую и в записи",
        url: "https://www.facebook.com/closerkiev/",
    },
    {
        heading: "KATACULT",
        description: "Стримы вживую и в записи",
        url: "https://www.facebook.com/katacult",
    },
    {
        heading: "CRIMINAL PRACTICE",
        description: "Онлайн-трансляции DJ-трио",
        url: "https://www.facebook.com/criminalpractice/",
    },
    {
        heading: "KULTURA ZVUKA",
        description: "Харьковская формация перенесла вечеринки в онлайн. Лайнапы обновляются.",
        url: "https://online.kulturazvuka.com.ua/",
    },
    {
        heading: "NIGHT AMBASSADORS",
        description: "Львовская электронная группировка переместилась в онлайн",
        url: "https://nightambassadors.com/",
    },
    {
        heading: "STAY",
        description: "Онлайн-концерты в телефоне или планшете. Сервис платный",
        url: "http://www.staystay.ru/",
    },
    {
        heading: "JAMALA",
        description: "Стримы хороших песен, можно смотреть в записи",
        url: "https://www.instagram.com/jamalajaaa/",
    },
    {
        heading: "THE HARDKISS",
        description: "Запись live-стрима из домашней студии. 50 минут хорошего настроения",
        url: "https://www.instagram.com/tv/B9_zbwmlpMO/?utm_source=ig_web_copy_link",
    },
    {
        heading: "ХВИЛЬОВИЙ",
        description: "Онлайн-вечеринки музыкального бара",
        url: "https://www.facebook.com/hvlvbar/",
    },
    {
        heading: "COLORS",
        description: "Музыкальное шоу запустило круглосуточный нонстоп-стрим",
        url: "https://www.youtube.com/watch?v=u4lxQlNyjY0&feature=emb_logo",
    },
    {
        heading: "WILL SMITH",
        description: "Полуторачасовой lo-fi chill beat от Уила",
        url: "https://www.youtube.com/watch?v=rA56B4JyTgI",
    },
    {
        heading: "CHILLED CROW",
        description: "Круглосуточный lo-fi хип-хоп стрим",
        url: "https://www.youtube.com/watch?v=5qap5aO4i9A",
    },
    {
        heading: "БОЛЬ",
        description: "Стрим-марафон музыкального фестиваля. Начало 25 марта, 12-00 мск",
        url: "http://karantinbol.com/",
    },
]



const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/EaZ7ig1'
    ).then(() => next())
}


tractorPodcastsScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Подкасты

🔎 Имя проекта: ${podCat[step].heading}
📝 Описание: ${podCat[step].description}
⛓ Ссылка: ${podCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorPodcastsScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == podCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${podCat[step].heading}
📝 Описание: ${podCat[step].description}
⛓ Ссылка: ${podCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorPodcastsScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${podCat[step].heading}
📝 Описание: ${podCat[step].description}
⛓ Ссылка: ${podCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorPodcastsScene.hears(``, enter(''))

tractorPodcastsScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorPodcastsScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorPodcastsScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorPodcastsScene