const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorBooksScene = new Scene('tractorBooks');
let step = 0;
const bookCat = [{
    heading: "Тут точно что-то будет",
    description: "И тут тоже появиться",
    url: "Все будет хорошо",
}, ]

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/pjqyXmx'
    ).then(() => next())
}


tractorBooksScene.enter(sendImg, (ctx) => ctx.reply(
    `Выбрана каттегория: Книги и журналы

🔎 Имя проекта: ${bookCat[step].heading}
📝 Описание: ${bookCat[step].description}
⛓ Ссылка: ${bookCat[step].url}


`, Markup
    .keyboard([
        [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
        [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))

tractorBooksScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == bookCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${bookCat[step].heading}
📝 Описание: ${bookCat[step].description}
⛓ Ссылка: ${bookCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorBooksScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${bookCat[step].heading}
📝 Описание: ${bookCat[step].description}
⛓ Ссылка: ${bookCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorBooksScene.hears(``, enter(''))

tractorBooksScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorBooksScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorBooksScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorBooksScene