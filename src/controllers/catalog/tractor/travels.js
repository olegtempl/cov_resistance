const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorTravelsScene = new Scene('tractorTravels');
let step = 0;
const traCat = [{
        heading: "GOOGLE MAPS",
        description: "Путешествия по миру, прогулки по известным улицам",
        url: "https://www.google.com/maps/about/treks/#/grid",
    },
    {
        heading: "AIRPANO",
        description: "Виртуальные путешествия вокруг света",
        url: "https://www.airpano.ru/",
    },
    {
        heading: "МКС",
        description: "Веб-камера на борту станции",
        url: "http://www.ustream.tv/channel/iss-hdev-payload",
    },
    {
        heading: "EARTHCAM",
        description: "Огромная база веб-камер со всего мира",
        url: "https://www.earthcam.com/",
    },
    {
        heading: "360CITIES",
        description: "Панорамы городов мира",
        url: "https://www.360cities.net/map",
    },
    {
        heading: "GEOSFERA",
        description: "Видеопутешествия по странам и городам мира",
        url: "https://geosfera.org/video/",
    },

]

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/sMYEvBe'
    ).then(() => next())
}


tractorTravelsScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Путешествия

🔎 Имя проекта: ${traCat[step].heading}
📝 Описание: ${traCat[step].description}
⛓ Ссылка: ${traCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorTravelsScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == traCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${traCat[step].heading}
📝 Описание: ${traCat[step].description}
⛓ Ссылка: ${traCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorTravelsScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${traCat[step].heading}
📝 Описание: ${traCat[step].description}
⛓ Ссылка: ${traCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorTravelsScene.hears(``, enter(''))

tractorTravelsScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorTravelsScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorTravelsScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorTravelsScene