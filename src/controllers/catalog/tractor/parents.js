const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const tractorParentsScene = new Scene('tractorParents');
let step = 0;
const parCat = [{
        heading: "KINDERUNI",
        description: "Немецкий детский онлайн-университет. Бесплатный образовательный проект Гёте-Института для детей в возрасте 8-12 лет",
        url: "https://www.goethe.de/ins/ru/ru/spr/eng/kin/kin.html",
    },
    {
        heading: "ФОКСФОРД",
        description: "Бесплатный доступ ко всем курсам по школьной программе на время карантина в школах",
        url: "https://help.foxford.ru/",
    },
    {
        heading: "ТЕТРИКА",
        description: "Онлайн-школа открыла бесплатный доступ к курсам подготовки к ЕГЭ и дарит индивидуальное онлайн-занятие с репетитором по промокоду QUARANTINE",
        url: "https://tetrika-school.ru/",
    },
    {
        heading: "РЕШИ-ПИШИ",
        description: "Онлайн-платформа с задачами на разные типы мышления для детей 3-9 лет",
        url: "https://reshi-pishi.ru/",
    },
    {
        heading: "EDERA",
        description: "Курсы для учителей и учеников, которые готовятся к ЗНО",
        url: "https://www.ed-era.com/courses/",
    },
    {
        heading: "ILEARN",
        description: "Курсы, вебинары и тесты к ЗНО",
        url: "https://ilearn.org.ua/",
    },
    {
        heading: "KHAN ACADEMY",
        description: "Бесплатные курсы для школьников",
        url: "https://uk.khanacademy.org/",
    },
    {
        heading: "РАДИО ARZAMAS",
        description: "Детская комната в приложении платформы. Бесплатный доступ по промокоду КАРАНТИН до 15 апреля",
        url: "https://arzamas.academy/radio",
    },
    {
        heading: "BE SMART",
        description: "Платформа для подготовки к ЗНО онлайн",
        url: "https://besmart.study/#section-courses",
    },
    {
        heading: "КИЕВ ДЛЯ ДЕТЕЙ",
        description: "Онлайн-прогулки с Юлией Бевзенко. 100 грн",
        url: "https://yuliabevzenko.com/ua/walks#online_walks",
    },
    {
        heading: "МОРЕ ПРИКЛЮЧЕНИЙ",
        description: "Домашний квест для детей 8-14 лет. Нужны ножницы и принтер",
        url: "https://drive.google.com/file/d/184cXy03fcGDQL94hIGOne0khFk00axN2/view?fbclid=IwAR3h_FsbAsIqoIK5RkP_narSpSzT7vhNasYBYhFRKqxvDV9LNJ4rlccMOog",
    },

]
const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/E4bwHCX'
    ).then(() => next())
}


tractorParentsScene.enter(sendImg, (ctx) => ctx.reply(
        `Выбрана каттегория: Образование

🔎 Имя проекта: ${parCat[step].heading}
📝 Описание: ${parCat[step].description}
⛓ Ссылка: ${parCat[step].url}


`, Markup
        .keyboard([
            [`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`],
            [`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
        ])
        .oneTime()
        .resize()
        .extra()
    )

)

tractorParentsScene.hears(`${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`, sendImg, (ctx) => {
    step++;

    if (step == parCat.length) {
        ctx.reply(`Конец списка. Можно отмотать назад и всё начать заново 😀`, Markup
            .keyboard([
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
            ])
            .oneTime()
            .resize()
            .extra())

    } else {
        ctx.reply(`
🔎 Имя проекта: ${parCat[step].heading}
📝 Описание: ${parCat[step].description}
⛓ Ссылка: ${parCat[step].url}



    `, Markup
            .keyboard([
                [
                    `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
                ]
            ])
            .oneTime()
            .resize()
            .extra())
    }
})

tractorParentsScene.hears(`${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`, sendImg, (ctx) => {
    step--;

    ctx.reply(`
🔎 Имя проекта: ${parCat[step].heading}
📝 Описание: ${parCat[step].description}
⛓ Ссылка: ${parCat[step].url}



        `, Markup
        .keyboard([
            [
                `${textData.eB.presentation.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.presentation.nextBtn} ${textData.menuText.nextBtn}`,
            ]
        ])
        .oneTime()
        .resize()
        .extra())
})




// tractorParentsScene.hears(``, enter(''))

tractorParentsScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn} в меню`, enter('tractor'))
tractorParentsScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorParentsScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default tractorParentsScene