const Scene = require('telegraf/scenes/base')

const {
    enter,
    leave
} = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const lots = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]

const catalogAgileConsumablesAccessoriesScene = new Scene('catalogAgileConsumAccessories'); //

let howManyLotsOnBd = lots.length,
    counterLots = 0;

let btnPanel = [],
    lotsForNextPost = [];

const sliceLots = (startNumberLot, ctx, next) => {
    lotsForNextPost = lots.slice(startNumberLot, startNumberLot + 5)
    return lotsForNextPost

}

let firstPost = (ctx, next) => {
    ctx.reply(lotsForNextPost[0]).then(() => next())
}

let secondPost = (ctx, next, ) => {
    ctx.reply(lotsForNextPost[1]).then(() => next())
}

let thirdPost = (ctx, next, ) => {
    ctx.reply(lotsForNextPost[2]).then(() => next())
}

let fourthPost = (ctx, next, ) => {
    ctx.reply(lotsForNextPost[3]).then(() => next())
}

let fifthPost = (ctx, next, ) => {
    ctx.reply(lotsForNextPost[4]).then(() => next())
}

let msg = '',
    firstStart = true;
let msgEnd = `Надеюсь вам успело что-нибудь приглянуться из того что я предложил, отлистайте вверх и нажмите добавить в корзину на приглянувемся товаре.`

// указать сколько доступно под заказ

catalogAgileConsumablesAccessoriesScene.enter((ctx) => {
    if (howManyLotsOnBd - counterLots <= 5) counterLots = counterLots + 5
    sliceLots(counterLots)

    if (firstStart) {
        counterLots = 0
        // console.log('1 уровень - 1 ступень')
        msg = `В наличии ${howManyLotsOnBd} позиций. Для открытия следующих 5 товаров нажми на кнопку ${textData.cmdText.store.printFiveLots} ${textData.eN.five} в правом углу.`
    }
    if (!firstStart && (counterLots != howManyLotsOnBd || counterLots < howManyLotsOnBd) && howManyLotsOnBd - counterLots > 5) {
        msg = `В наличии ${howManyLotsOnBd} позиций, до конца списка осталось ${howManyLotsOnBd - counterLots}. Для открытия следующих 5 товаров нажми на кнопку ${textData.cmdText.store.printFiveLots} ${textData.eN.five } в правом углу.`
        // console.log('1 уровень - 2 ступень')

    }
    if (counterLots == howManyLotsOnBd || howManyLotsOnBd - counterLots < 5 || counterLots > howManyLotsOnBd) {
        msg = `В наличии ${howManyLotsOnBd} позиций. Для открытия последних позиций в списке нажми на кнопку ${textData.cmdText.store.printFiveLots} ${textData.eN.five} в правом углу.`
        // console.log('1 уровень - 3 ступень')

    }
    firstStart = false

    if (howManyLotsOnBd > 5 && (counterLots == howManyLotsOnBd || counterLots < howManyLotsOnBd)) {
        btnPanel = [`${textData.eB.backBtn}`, `${textData.eB.goMainMenuBtn}`, `${textData.eB.store}`, `${textData.cmdText.store.printFiveLots} ${textData.eN.five}`]

        ctx.reply(`${msg}`, Markup
            .keyboard([
                btnPanel
            ])
            .oneTime()
            .resize()
            .extra()
        )
        if (howManyLotsOnBd - counterLots > 5) counterLots = counterLots + 5

    }
    if (howManyLotsOnBd < 5 || counterLots > howManyLotsOnBd) {
        btnPanel = [`${textData.eB.backBtn}`, `${textData.eB.goMainMenuBtn}`, `${textData.eB.store}`]
        ctx.reply(`${endMsg}`, Markup
            .keyboard([
                btnPanel
            ])
            .oneTime()
            .resize()
            .extra()
        )


    }

})

catalogAgileConsumablesAccessoriesScene.hears(`${textData.cmdText.store.printFiveLots} ${textData.eN.five}`, firstPost, secondPost, thirdPost, fourthPost, fifthPost, (ctx) => {
    sliceLots(counterLots)
    // console.log(howManyLotsOnBd - counterLots + " самый верх")


    if (!firstStart && (counterLots != howManyLotsOnBd || counterLots < howManyLotsOnBd) && howManyLotsOnBd - counterLots > 5) {
        msg = `В наличии ${howManyLotsOnBd} позиций, до конца списка осталось ${howManyLotsOnBd - counterLots}. Для открытия следующих 5 товаров нажми на кнопку ${textData.cmdText.store.printFiveLots} ${textData.eN.five} в правом углу.`
        // console.log('2 уровень - 1 ступень')

    }
    if (counterLots == howManyLotsOnBd || howManyLotsOnBd - counterLots < 5 || counterLots > howManyLotsOnBd) {
        msg = `В наличии ${howManyLotsOnBd} позиций. Для открытия последних позиций в списке нажми на кнопку ${textData.cmdText.store.printFiveLots} ${textData.eN.five} в правом углу.`
        // console.log('2 уровень - 2 ступень')
    }
    if (counterLots != howManyLotsOnBd || counterLots < howManyLotsOnBd || howManyLotsOnBd - counterLots > 5) {
        btnPanel = [`${textData.eB.backBtn}`, `${textData.eB.goMainMenuBtn}`, `${textData.eB.store}`, `${textData.cmdText.store.printFiveLots} ${textData.eN.five}`]
        // console.log(howManyLotsOnBd - counterLots + ' начало 2 функции')

        ctx.reply(`${msg}`, Markup
            .keyboard([
                btnPanel
            ])
            .oneTime()
            .resize()
            .extra()
        )
        // console.log(howManyLotsOnBd - counterLots + " конец функции")
        counterLots = counterLots + 5

    } else {
        btnPanel = [`${textData.eB.backBtn}`, `${textData.eB.goMainMenuBtn}`, `${textData.eB.store}`]
        ctx.reply(`${msgEnd}`, Markup
            .keyboard([
                btnPanel
            ])
            .oneTime()
            .resize()
            .extra()
        )
    }

})


catalogAgileConsumablesAccessoriesScene.hears(`${textData.eB.sto.header}`, enter('store'))
catalogAgileConsumablesAccessoriesScene.hears(`${textData.eB.backBtn}`, enter('catalogAgileConsum'))
catalogAgileConsumablesAccessoriesScene.hears(`${textData.eB.goMainMenuBtn}`, enter('mainMenu'))

export default catalogAgileConsumablesAccessoriesScene