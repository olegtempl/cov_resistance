const Scene = require('telegraf/scenes/base')
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const {
    enter,
    leave
} = Stage


const tractorScene = new Scene('tractor');

const sendImg = (ctx, next) => {
    ctx.replyWithPhoto(
        'https://imgur.com/Mz8JqCc'
    ).then(() => next())
}



tractorScene.enter(sendImg, (ctx) => ctx.reply(
    `Социальный проект - Беларуский трактор.

Команда проекта DarkDev brand вносиn сюда актуальную информацию о любом здоровом предложении беларуско говорящему обществу и не только.

Библиотека это трактор, чтобы помочь людям выехать.
Если в библиотеке нет нужной категории, мы добавим ее.
Устаревшую информацию поменяем.

Это бесплатно. Это интересно. Эта библиотека для тех, кому нужна помощь. Здесь собраны обучение, балет, доставка еды, онлайн-бары и все, что можно получить в онлайн - кстати сюда войдут проекты с этого и хакатонов в других странах. 95% предложений бесплатны.`, Markup
    .keyboard([
        [
            '🎓 Образование', '🎙 Подкасты'
        ],
        [
            '🎨 Искусство', '🏖 Путешествия'
        ],
        [
            '🥂 Онлайн-Бары', '📡 Мониторинг'
        ],
        [
            '🎎 Родителям', '📖 Книги и журналы'
        ],
        [
            '💊 Медицина', '🍣 Доставка еды'
        ],
        [
            '🎥 Кино', '⚽️ Спорт'
        ],
        [`${textData.eB.backBtn} ${textData.menuText.backBtn}`, `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))

tractorScene.hears(`🎓 Образование`, enter('tractorEducation'))
tractorScene.hears(`🎙 Подкасты`, enter('tractorPodcasts'))
tractorScene.hears(`🎨 Искусство`, enter('tractorArt'))
tractorScene.hears(`🏖 Путешествия`, enter('tractorTravels'))
tractorScene.hears('🥂 Онлайн-Бары', enter('tractorBars'))
tractorScene.hears(`🎎 Родителям`, enter('tractorParents'))
tractorScene.hears(`📡 Мониторинг`, enter('tractorMonitoring'))
tractorScene.hears(`🎓 📖 Книги и журналы`, enter('tractorBooks'))
tractorScene.hears(`🍣 Доставка еды`, enter('tractorEat'))
tractorScene.hears(`🎥 Кино`, enter('tractorKino'))
tractorScene.hears(`⚽️ Спорт`, enter('tractorSport'))




tractorScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('catalogStart'))
tractorScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))
// tractorScene.on('message', (ctx) => ctx.reply(msgText.ifUserSendMsgVicePressBtn))

export default tractorScene