const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const adminMainMenuScene = new Scene('adminMainMenu');

adminMainMenuScene.enter(
    (ctx) => ctx.reply(
        textData.cmdText.admin.header, Markup
            .keyboard([
                [`${textData.eB.eve.header} ${textData.menuText.admin.events.header}`,
                    `${textData.eB.admBtn.connect.header} ${textData.menuText.admin.connect.header}`],
                [`${textData.eB.set.header} ${textData.menuText.admin.settings.header}`],
                [`${textData.eB.backBtn} ${textData.menuText.backBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)

adminMainMenuScene.hears(`${textData.eB.eve.header} ${textData.menuText.admin.events}`, enter('adminEvents'))
adminMainMenuScene.hears(`${textData.eB.admBtn.connect} ${textData.menuText.admin.connect}`, enter('adminConnect'))
adminMainMenuScene.hears(`${textData.eB.set.header} ${textData.menuText.admin.settings}`, enter('adminSettings'))
adminMainMenuScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminMainMenuScene