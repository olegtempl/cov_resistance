const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'

const adminNewEventScene = new Scene('adminNewEvent');
/* 
    description: yes 
    formate: yes
    type: ''
    town: yes
    place: yes
    timeStart: "", // указать формат и пример
    timeEnd: "",
    donate: yes
    сontactOrganisator: yes
    facilitator: допилить  2 кнопок с цифрами для выбора
    organisator:  yes
    whyCome: yes
    date: "", допилить обработку полученного текста whycome перед записью значения в переменную  
    maxPeople: yes
    url: допилить обработку полученного текста url перед записью значения в переменную
           validate info
    edit info       registration
 */
adminNewEventScene .enter(
    (ctx) => ctx.reply(
        `${textData.cmdText.admin.events.new.header}`, Markup
            .keyboard([
                [`${textData.eB.backBtn}`,
                    `${textData.eB.goMainMenuBtn}`,
                    `${textData.eB.nextBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)

adminNewEventScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventDescription'))

adminNewEventScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminEvents'))
adminNewEventScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventScene 