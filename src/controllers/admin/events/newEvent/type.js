const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'

const adminNewEventTypeScene = new Scene('adminNewEventType');

adminNewEventTypeScene.enter(
    (ctx) => {

        ctx.reply(
            `${textData.cmdText.admin.events.new.type}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                    `${textData.eN.two}`,
                    `${textData.eN.three}`,
                    // `${textData.eN.four}`,
                    // `${textData.eN.five}`
                ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                        // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)



const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.type, '1').then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.type, '2').then(() => next())
const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.type, '3').then(() => next())
// const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '4').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '5').then(() => next())

adminNewEventTypeScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventTown'))
adminNewEventTypeScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventTown'))
adminNewEventTypeScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventTown'))
// adminNewEventTypeScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventTown'))
// adminNewEventTypeScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventTown'))

adminNewEventTypeScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventFormate'))
adminNewEventTypeScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventTypeScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventTypeScene 