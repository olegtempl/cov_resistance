const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventTimeStartScene = new Scene('adminNewEventTimeStart');

adminNewEventTimeStartScene.enter(
    (ctx) => {
        // if (ctx.message.text.length != textData.eB.nextBtn) ctx.newEvent.timeStart = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.timeStart}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                        // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

// adminNewEventTimeStartScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventEnd'))

adminNewEventTimeStartScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventPlace'))
adminNewEventTimeStartScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventTimeStartScene.on('message', enter('adminNewEventTimeEnd'))

export default adminNewEventTimeStartScene 