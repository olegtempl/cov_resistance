const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'

const adminNewEventContactOrganisatorScene = new Scene('adminNewEventContactOrganisator');

adminNewEventContactOrganisatorScene.enter(
    (ctx) => {
        ctx.reply(
            `${textData.cmdText.admin.events.new.сontactOrganisator}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                        `${textData.eN.two}`,
                        // `${textData.eN.three}`,
                        // `${textData.eN.four}`,
                        // `${textData.eN.five}`
                    ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.сontactOrganisator, '+375259654011').then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.сontactOrganisator, '+380939194213').then(() => next())
// const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.сontactOrganisator, '+375259654011').then(() => next())
// const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.сontactOrganisator, '+375259654011').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.сontactOrganisator, '+375259654011').then(() => next())

adminNewEventContactOrganisatorScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventFacilitator'))
adminNewEventContactOrganisatorScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventFacilitator'))
// adminNewEventContactOrganisatorScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventFacilitator'))
// adminNewEventContactOrganisatorScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventFacilitator'))
// adminNewEventContactOrganisatorScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventFacilitator'))
adminNewEventContactOrganisatorScene.hears(`${textData.eB.nextBtn}`,  enter('adminNewEventFacilitator'))

adminNewEventContactOrganisatorScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventDonate'))
adminNewEventContactOrganisatorScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventContactOrganisatorScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventContactOrganisatorScene 