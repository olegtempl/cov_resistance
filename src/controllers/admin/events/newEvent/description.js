const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventDescriptionScene = new Scene('adminNewEventDescription');

adminNewEventDescriptionScene.enter(
    (ctx) => {
        ctx.newEvent.name = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.description}`, Markup
            .keyboard([
                [`${textData.eB.backBtn}`,
                `${textData.eB.goMainMenuBtn}`,
                `${textData.eB.nextBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )}
)

adminNewEventDescriptionScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventFormate'))

adminNewEventDescriptionScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEvent'))
adminNewEventDescriptionScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventDescriptionScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventDescriptionScene 