const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'

const adminNewEventFacilitatorScene = new Scene('adminNewEventFacilitator');

adminNewEventFacilitatorScene.enter(
    (ctx) => {
        ctx.reply(
            `${textData.cmdText.admin.events.new.faciltator}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                    `${textData.eN.two}`,
                    // `${textData.eN.three}`,
                    // `${textData.eN.four}`,
                    // `${textData.eN.five}`
                ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)



const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, "Oleg Medvedev").then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, "Anrey Litkin").then(() => next())
// const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '3').then(() => next())
// const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '4').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '5').then(() => next())

adminNewEventFacilitatorScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventOrganisator'))
adminNewEventFacilitatorScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventOrganisator'))
// adminNewEventFacilitatorScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventOrganisator'))
// adminNewEventFacilitatorScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventOrganisator'))
// adminNewEventFacilitatorScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventOrganisator'))

adminNewEventFacilitatorScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventContactOrganisator'))
adminNewEventFacilitatorScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventFacilitatorScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventFacilitatorScene 