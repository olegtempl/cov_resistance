const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'


const adminNewEventDonateScene = new Scene('adminNewEventDonate');

adminNewEventDonateScene.enter(
    (ctx) => {
        ctx.newEvent.timeEnd = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.donate}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                    `${textData.eN.two}`,
                        `${textData.eN.three}`,
                        `${textData.eN.four}`,
                        // `${textData.eN.five}`
                    ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.donate, "Бесплатно").then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.donate, '8').then(() => next())
const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.donate, '25').then(() => next())
const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.donate, '40').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.donate, '+375259654011').then(() => next())

adminNewEventDonateScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventContactOrganisator'))
adminNewEventDonateScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventContactOrganisator'))
adminNewEventDonateScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventContactOrganisator'))
adminNewEventDonateScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventContactOrganisator'))
// adminNewEventDonateScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventContactOrganisator'))


adminNewEventDonateScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventTimeEnd'))
adminNewEventDonateScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventDonateScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventDonateScene 