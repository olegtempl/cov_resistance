const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventWhyComeScene = new Scene('adminNewEventWhyCome');

adminNewEventWhyComeScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) ctx.newEvent.organisator = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.whyCome}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

// adminNewEventWhyComeScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventDate'))

adminNewEventWhyComeScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewOrganisator'))
adminNewEventWhyComeScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventWhyComeScene.on('message', enter('adminNewEventDate'))

export default adminNewEventWhyComeScene 