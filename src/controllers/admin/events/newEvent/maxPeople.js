const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventMaxPeopleScene = new Scene('adminNewEventMaxPeople');

adminNewEventMaxPeopleScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) ctx.newEvent.date = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.maxPeople}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.goMainMenuBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.backBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

//adminNewEventMaxPeopleScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventUrl'))

adminNewEventMaxPeopleScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventDate'))
adminNewEventMaxPeopleScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventMaxPeopleScene.on('message', enter('adminNewEventUrl'))

export default adminNewEventMaxPeopleScene 