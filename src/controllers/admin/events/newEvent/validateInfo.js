const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import calcIndexEvent from '../../../../utils/calcIndexEvent'

const adminNewEventValidateInfoScene = new Scene('adminNewEventValidateInfo');
/* 
    ToDo:
        * публикация информации о новом мероприятии в чатах команды и комьюнити
        * доделать парсер текстаы
        * присвоение ctx.newEvent.eventEnd = "no",

*/
adminNewEventValidateInfoScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) {
            let temp = ctx.message.text

            // temp.relace() замена: .пробел на без точки + перенос строки 
            ctx.newEvent.url = ctx.message.text;
        } 

        ctx.newEvent.indexEvent = calcIndexEvent(ctx.newEvent.type, ctx.newEvent.formate)

ctx.reply(`${textData.eT.admin.events.new.validateInfo} ${textData.cmdText.admin.events.new.validateInfo}

${textData.cmdText.events.upcomingEvents}
${textData.eT.events.upcomingEvents} ${formateDate('ru', ctx.newEvent.date)}
${textData.eT.events.nameEvent} ${ctx.newEvent.name} ${ctx.newEvent.indexEvent}

${textData.eT.events.timeSpending} ${textData.msgText.events.timeSpending}: ${textData.msgText.events.from} ${ctx.newEvent.timeStart} ${textData.msgText.events.before} ${ctx.newEvent.timeEnd}
${textData.eT.events.place} ${textData.msgText.events.place}: ${ctx.newEvent.town} ${ctx.newEvent.place}
${textData.eT.events.donate} ${textData.msgText.events.donate}: ${ctx.newEvent.donate}
        
${textData.eT.events.formate} ${textData.msgText.events.formate}: ${ctx.newEvent.formate}

${shortDescription(ctx.newEvent.description)}

${textData.eT.events.placesLeft} ${textData.msgText.events.placesLeft}: ${ctx.newEvent.maxPeople} 

${textData.eT.events.whyCome} ${textData.msgText.events.whyCome}
${printArrayWithList(ctx.newEvent.whyCome)}

Facebook: ${ctx.newEvent.url[0]}
LinkedIn: ${ctx.newEvent.url[1]}

${textData.eT.events.facilitator} ${textData.msgText.events.facilitator}: ${ctx.newEvent.facilitator}

${textData.eT.events.org} ${textData.msgText.events.org}: ${ctx.newEvent.organisator}
${textData.eT.events.contactOrganisator} ${textData.msgText.events.contactOrganisator}: ${ctx.newEvent.contactOrganisator}

${textData.cmdText.admin.events.new.validateError} ${textData.eB.admin.events.new.editInfo} ${textData.menuText.admin.events.new.editInfo} 
${textData.cmdText.admin.events.new.validateGood} ${textData.eB.admin.events.new.validateGood} ${textData.menuText.admin.events.new.validateGood}
            `, Markup
                .keyboard([
                    [`${textData.eB.admin.events.new.editInfo} ${textData.menuText.admin.events.new.editInfo}`,
                    `${textData.eB.admin.events.new.validateGood} ${textData.menuText.admin.events.new.validateGood}`
                    ],
                    [`${textData.eB.backBtn}`,
                    `${textData.eB.goMainMenuBtn}`],
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

adminNewEventValidateInfoScene.hears(`${textData.eB.admin.events.new.editInfo} ${textData.menuText.admin.events.new.editInfo}`, enter('adminNewEventEditInfo'))
adminNewEventValidateInfoScene.hears(`${textData.eB.admin.events.new.validateGood} ${textData.menuText.admin.events.new.validateGood}`, enter('adminNewEventRegistration'))

adminNewEventValidateInfoScene.hears(`${textData.eB.backBtn}`, enter('adminNewEventMaxPeople'))
adminNewEventValidateInfoScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
// adminNewEventValidateInfoScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventValidateInfoScene 