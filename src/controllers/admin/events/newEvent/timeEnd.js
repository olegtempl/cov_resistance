const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventTimeEndScene = new Scene('adminNewEventTimeEnd');

adminNewEventTimeEndScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) ctx.newEvent.timeStart = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.timeEnd}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

// adminNewEventTimeEndScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventEnd'))

adminNewEventTimeEndScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventTimeStart'))
adminNewEventTimeEndScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventTimeEndScene.on('message', enter('adminNewEventDonate'))

export default adminNewEventTimeEndScene 