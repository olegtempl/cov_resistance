const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'

const adminNewEventPlaceScene = new Scene('adminNewEventPlace');

adminNewEventPlaceScene.enter(
    (ctx) => {
        ctx.reply(
            `${textData.cmdText.admin.events.new.place}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                    // `${textData.eN.two}`,
                    // `${textData.eN.three}`,
                    // `${textData.eN.four}`,
                    // `${textData.eN.five}`
                ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)


const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.place, 'адрес айтиландии на институте').then(() => next())
// const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.place, '2').then(() => next())
// const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.place, '3').then(() => next())
// const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.place, '4').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.place, '5').then(() => next())

adminNewEventPlaceScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventTimeStart'))
// adminNewEventPlaceScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventTimeStart'))
// adminNewEventPlaceScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventTimeStart'))
// adminNewEventPlaceScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventTimeStart'))
// adminNewEventPlaceScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventTimeStart'))

// adminNewEventPlaceScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventTimeStart'))

adminNewEventPlaceScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventTown'))
adminNewEventPlaceScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventPlaceScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventPlaceScene 