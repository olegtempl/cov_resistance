const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventOrganisatorScene = new Scene('adminNewEventOrganisator');

adminNewEventOrganisatorScene.enter(
    (ctx) => {
        ctx.reply(
            `${textData.cmdText.admin.events.new.organisator}
${textData.cmdText.admin.events.new.defaultValue} ${ctx.newEvent.organisator}
${textData.cmdText.admin.events.new.enterNewValue} ${textData.eB.nextBtn}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn}`,
                    `${textData.eB.goMainMenuBtn}`,
                    `${textData.eB.nextBtn}`]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

adminNewEventOrganisatorScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventWhyCome'))

adminNewEventOrganisatorScene.hears(`${textData.eB.backBtn}`, enter('adminNewFacilitator'))
adminNewEventOrganisatorScene.hears(`${textData.eB.goMainMenuBtn}`, enter('adminMainMenu'))
// adminNewEventOrganisatorScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventOrganisatorScene 