const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setData from '../../../../utils/sqliteConnectors/setData'

const adminNewEventRegistrationScene = new Scene('adminNewEventRegistration');
/* 
    ToDo:
        * После оконательной регистрации прислать изображеньку какую нибудь 
        * создание дирректории с индексом ивента по пути data/filesForEvents/ - отсюда будут браться файлы для мероприятия
*/

let { 
    name,
    contactOrganisator,
    donate,
    formate,
    place,
    town,
    date,
    eventEnd,
    maxPeople,
    timeEnd,
    url,
    description,
    facilitator,
    organisator,
    timeStart,
    whyCome } = ctx.newEvent




const addNewEventOnDb = () => {
    cl.log(setData('events',
        ['name', 'description', 'donate', 'maxPeole', 'place', 'town', 'date', 'facilitator', 'timeStart', 'timeEnd', 'formate', 'contactOrganisator', 'organisator', 'whyCome', 'url', 'eventEnd', 'indexEvent'],
        `${name}, ${description}, ${donate}, ${maxPeople}, ${place}, ${town}, ${date}, ${facilitator}, ${timeStart}, ${timeEnd}, ${formate}, ${contactOrganisator}, ${organisator}, ${whyCome}, ${url}, ${eventEnd}, ${indexEvent}`))}

adminNewEventRegistrationScene.enter(
    (ctx) => {
        let promiseAddNewEventOnDb = new Promise(function (resolve, reject) {
            resolve(addNewEventOnDb())
        });
        


        promiseAddNewEventOnDb.then(() => {
            result => {
                console.log(result)
                ctx.reply(
                    `${textData.eT.admin.events.new.end} ${textData.cmdText.admin.events.new.end}`, Markup
                    .keyboard([
                        [`${textData.eB.goMainMenuBtn}`, `${textData.menuText.goMainMenuBtn}`
                        ]
                    ])
                    .oneTime()
                    .resize()
                    .extra()
            )},
                error => {
                    console.log(error)
                    ctx.reply(
                    `${textData.msgText.error}
${error}`, Markup
                        .keyboard([
                            [`${textData.eB.goMainMenuBtn}`, `${textData.menuText.goMainMenuBtn}`
                            ]
                        ])
                        .oneTime()
                        .resize()
                        .extra()
                )}           
        })

    }
)

adminNewEventRegistrationScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventRegistrationScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventRegistrationScene 