const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventUrlScene = new Scene('adminNewEventUrl');

adminNewEventUrlScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) ctx.newEvent.maxPeople = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.url}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

// adminNewEventUrlScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventEnd'))

adminNewEventUrlScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventMaxPeople'))
adminNewEventUrlScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventUrlScene.on('message', enter('adminNewEventEnd'))

export default adminNewEventUrlScene 