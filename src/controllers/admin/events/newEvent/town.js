const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'
import setValueOnObject from '../../../../utils/setValueOnObject'


const adminNewEventTownScene = new Scene('adminNewEventTown');

adminNewEventTownScene.enter(
    (ctx) => {
        ctx.reply(
            `${textData.cmdText.admin.events.new.town}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                        `${textData.eN.two}`,
                        // `${textData.eN.three}`,
                        // `${textData.eN.four}`,
                        // `${textData.eN.five}`
                    ],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    //`${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)


const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.town, 'Минск').then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.town, 'Херсон').then(() => next())
// const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.town, '3').then(() => next())
// const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.town, '4').then(() => next())
// const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.town, '5').then(() => next())

adminNewEventTownScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventPlace'))
adminNewEventTownScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventPlace'))
// adminNewEventTownScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventPlace'))
// adminNewEventTownScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventPlace'))
// adminNewEventTownScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventPlace'))

// adminNewEventTownScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventPlace'))

adminNewEventTownScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventType'))
adminNewEventTownScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventTownScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventTownScene 