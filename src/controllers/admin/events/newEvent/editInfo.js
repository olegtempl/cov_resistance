const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventValidateInfoScene = new Scene('adminNewEventValidateInfo');
/* 
    ToDo:
        * прислать полученный результат для проверки регистратору, и 2 кнопки: правка и далее
            * если что не так - нажимает правка (вставить кнопок с цифрами чтобы можно было вернуться на номер шага нажатой цифры - цифра переход на тот шаг - потом возврат к конечной сцене с двумя кнопками)
            * если все нормально то кнопку далее - окончание регистрации
        * После оконательной регистрации прислать изображеньку какую нибудь  и отослать данные и обьекта 
            ctx.newEvent.contactOrganisator
            ctx.newEvent.donate
            ctx.newEvent.formate
            ctx.newEvent.place
            ctx.newEvent.town
            ctx.newEvent.date
            ctx.newEvent.eventEnd
            ctx.newEvent.maxPeople
            ctx.newEvent.timeEnd
            ctx.newEvent.timeStart
            ctx.newEvent.url
            ctx.newEvent.description
            ctx.newEvent.facilitator
            ctx.newEvent.organisator
            ctx.newEvent.whyCome
        * написать sql запрос для сохранения нового ивента
*/
adminNewEventValidateInfoScene.enter(
    (ctx) => {

        ctx.reply(`${textData.cmdText.admin.events.new.editInfo}

${textData.eT.events.upcomingEvents} ${textData.msgText.events.date} - ${textData.eN.extra.one}
${textData.eT.events.nameEvent} ${textData.msgText.events.nameEvent} - ${textData.eN.extra.two}
${textData.eT.events.timeStart} ${textData.msgText.events.timeStart} - ${textData.eN.extra.three}
${textData.eT.events.timeEnd} ${textData.msgText.events.timeEnd} - ${textData.eN.extra.four}
${textData.eT.events.place} ${textData.msgText.events.place} - ${textData.eN.extra.five}
${textData.eT.events.town} ${textData.msgText.events.town} - ${textData.eN.extra.six}
${textData.eT.events.donate} ${textData.msgText.events.donate} - ${textData.eN.extra.seven}        
${textData.eT.events.formate} ${textData.msgText.events.formate} - ${textData.eN.extra.eight}
${textData.eT.events.description} ${textData.msgText.events.description} - ${textData.eN.extra.nine}
${textData.eT.events.maxPeople} ${textData.msgText.events.maxPeople} - ${textData.eN.extra.ten}
${textData.eT.events.whyCome} ${textData.msgText.events.whyCome} - ${textData.eN.extra.one}${textData.eN.extra.one}
${textData.eT.events.url} ${textData.msgText.events.url} - ${textData.eN.extra.one}${textData.eN.extra.two}
${textData.eT.events.facilitator} ${textData.msgText.events.facilitator} - ${textData.eN.extra.one}${textData.eN.extra.three}
${textData.eT.events.org} ${textData.msgText.events.org} - ${textData.eN.extra.one}${textData.eN.extra.four}
${textData.eT.events.contactOrganisator} ${textData.msgText.events.contactOrganisator} - ${textData.eN.extra.one}${textData.eN.extra.five}
${textData.eT.events.organisator} ${textData.msgText.events.organisator} - ${textData.eN.extra.one}${textData.eN.extra.six}
`, Markup
    .keyboard([
        [`${textData.eB.fee.faq.questionNumbers.firstQuestion}`, `${textData.eB.fee.faq.questionNumbers.secondQuestion}`, `${textData.eB.fee.faq.questionNumbers.thirdQuestion}`, `${textData.eB.fee.faq.questionNumbers.fourthQuestion}`, `${textData.eB.fee.faq.questionNumbers.fifthQuestion}`, `${textData.eB.fee.faq.questionNumbers.sixthQuestion}`, `${textData.eB.fee.faq.questionNumbers.seventhQuestion}`, `${textData.eB.fee.faq.questionNumbers.eigthQuestion}`, `${textData.eB.fee.faq.questionNumbers.ninthQuestion}`],
        [`${textData.eB.fee.faq.questionNumbers.tenthQuestion}`, `${textData.eB.fee.faq.questionNumbers.eleventhQuestion}`, `${textData.eB.fee.faq.questionNumbers.twelfthQuestion}`, `${textData.eB.fee.faq.questionNumbers.thirteethQuestion}`, `${textData.eB.fee.faq.questionNumbers.forteethQuestion}`, `${textData.eB.fee.faq.questionNumbers.fifteenthQuestion}`, `${textData.eB.fee.faq.questionNumbers.sixteenthQuestion}`],
        [`${textData.eB.fee.faq.questionNumbers.seventeenQuestion}`], 
        // `${textData.eB.fee.faq.questionNumbers.eighteenQuestion}`, `${textData.eB.fee.faq.questionNumbers.nineteethQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethFirstQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethSecondQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethThirdQuestion}`],
        // [`${textData.eB.fee.faq.questionNumbers.tweintiethFourthQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethFifthQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethSixthQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethSeventhQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethEigthQuestion}`, `${textData.eB.fee.faq.questionNumbers.tweintiethNinthQuestion}`, `${textData.eB.fee.faq.questionNumbers.thirtiethQuestion}`],

        [`${textData.eB.backBtn}`,
        `${textData.eB.goMainMenuBtn}`,
        // `${textData.eB.nextBtn}`
    ]
    ])
    .oneTime()
    .resize()
    .extra()
)
}
)
// adminNewEventValidateInfoScene.hears()
// adminNewEventValidateInfoScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventEnd'))

adminNewEventValidateInfoScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventValidateInfo'))
adminNewEventValidateInfoScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventValidateInfoScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventValidateInfoScene 