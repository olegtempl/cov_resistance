const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventDateScene = new Scene('adminNewEventDate');

adminNewEventDateScene.enter(
    (ctx) => {
        if (ctx.message.text.length != textData.eB.nextBtn) {
            let temp = ctx.message.text
            // воможно сделать отдельной функцией в utils
            // temp.relace() замена: .пробел на без точки + перенос строки 
            ctx.newEvent.whyCome = ctx.message.text;
        } 

        ctx.reply(
            `${textData.cmdText.admin.events.new.date}`, Markup
                .keyboard([
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                    ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)

// adminNewEventDateScene.hears(`${textData.eB.nextBtn}`, enter('adminNewEventMaxPeople'))

adminNewEventDateScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewWhyCome'))
adminNewEventDateScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventDateScene.on('message', enter('adminNewEventMaxPeople'))

export default adminNewEventDateScene 