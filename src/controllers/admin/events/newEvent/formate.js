const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../../utils/exportTextData'

const adminNewEventFormateScene = new Scene('adminNewEventFormate');

adminNewEventFormateScene.enter(
    (ctx) => {
        ctx.newEvent.description = ctx.message.text;

        ctx.reply(
            `${textData.cmdText.admin.events.new.formate}`, Markup
                .keyboard([
                    [`${textData.eN.one}`,
                    `${textData.eN.two}`,
                    `${textData.eN.three}`,
                    `${textData.eN.four}`,
                    `${textData.eN.five}`],
                    [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                    `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`,
                    // `${textData.eB.nextBtn}`
                ]
                ])
                .oneTime()
                .resize()
                .extra()
        )
    }
)


import setValueOnObject from '../../../../utils/setValueOnObject'

const afterClickFirstBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, "1").then(() => next())
const afterClickSecondBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '2').then(() => next())
const afterClickThirthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '3').then(() => next())
const afterClickFourthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '4').then(() => next())
const afterClickFifthBtn = (ctx, next) => setValueOnObject(ctx.newEvent.formate, '5').then(() => next())

adminNewEventFormateScene.hears(`${textData.eN.one}`, afterClickFirstBtn, enter('adminNewEventType'))
adminNewEventFormateScene.hears(`${textData.eN.two}`, afterClickSecondBtn, enter('adminNewEventType'))
adminNewEventFormateScene.hears(`${textData.eN.three}`, afterClickThirthBtn, enter('adminNewEventType'))
adminNewEventFormateScene.hears(`${textData.eN.four}`, afterClickFourthBtn, enter('adminNewEventType'))
adminNewEventFormateScene.hears(`${textData.eN.five}`, afterClickFifthBtn, enter('adminNewEventType'))

adminNewEventFormateScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminNewEventDescription'))
adminNewEventFormateScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNewEventFormateScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNewEventFormateScene 