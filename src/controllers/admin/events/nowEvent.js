/* 
    Выводим список ближайших мероприятий (взять модуль из пользовательского режима)

*/

const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../../utils/exportTextData'
import setValueOnObject from '../../../utils/setValueOnObject'
import requestFunc from '../../../utils/sqliteConnectors/requestFunc'
import eventsSqlRequests from '../../../utils/sqliteConnectors/events/sqlRequests'

/* 
    ToDo:
        * получаем из БД первые 3 события, записываем их значения в эти 3 переменные 
        * thirthNowEvent 
        * firstEvent 
        * secondNowEvent
*/
const adminNowEventScene = new Scene('adminNowEvent');

adminNowEventScene.enter(
    (ctx) => ctx.reply(
        textData.cmdText.admin.events.now.header, Markup
            .keyboard([
                [`${textData.eB.fee.faq.questionNumbers.firstQuestion}`, 
                `${textData.eB.fee.faq.questionNumbers.secondQuestion}`, 
                `${textData.eB.fee.faq.questionNumbers.thirdQuestion}`],
                [`${textData.eB.backBtn} ${textData.menuText.backBtn}`,
                `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)
const getFirstThreeEventOnBottomList = () => {
    let allEvents = requestFunc(eventsSqlRequests.main);

}
const setFirstIndexNowEvent = (ctx, next) => setValueOnObject(ctx.nowEvent.first, '1').then(() =>next())
const setSecondIndexNowEvent = (ctx, next) => setValueOnObject(ctx.nowEvent.second, '1').then(() =>next())
const setThirthIndexNowEvent = (ctx, next) => setValueOnObject(ctx.nowEvent.thirth, '1').then(() =>next())
// [`${textData.eB.fee.faq.questionNumbers.firstQuestion}`, `${textData.eB.fee.faq.questionNumbers.secondQuestion}`, `${textData.eB.fee.faq.questionNumbers.thirdQuestion}`, `${textData.eB.fee.faq.questionNumbers.fourthQuestion}`, `${textData.eB.fee.faq.questionNumbers.fifthQuestion}`, `${textData.eB.fee.faq.questionNumbers.sixthQuestion}`, `${textData.eB.fee.faq.questionNumbers.seventhQuestion}`, `${textData.eB.fee.faq.questionNumbers.eigthQuestion}`, `${textData.eB.fee.faq.questionNumbers.ninthQuestion}`],
//     [`${textData.eB.fee.faq.questionNumbers.tenthQuestion}`, `${textData.eB.fee.faq.questionNumbers.eleventhQuestion}`, `${textData.eB.fee.faq.questionNumbers.twelfthQuestion}`, `${textData.eB.fee.faq.questionNumbers.thirteethQuestion}`, `${textData.eB.fee.faq.questionNumbers.forteethQuestion}`, `${textData.eB.fee.faq.questionNumbers.fifteenthQuestion}`, `${textData.eB.fee.faq.questionNumbers.sixteenthQuestion}`],
//     [`${textData.eB.fee.faq.questionNumbers.seventeenQuestion}`
// adminNowEventScene.hears(`${textData.eB.admBtn.events.now.header} ${textData.menuText.events.now.header}`, enter('adminNowEvent'))
adminNowEventScene.hears(textData.eB.fee.faq.questionNumbers.firstQuestion, setFirstIndexNowEvent, ('adminNowfirstEvent'))
adminNowEventScene.hears(textData.eB.fee.faq.questionNumbers.secondQuestion, setSecondIndexNowEvent, ('adminNowsecondEvent'))
adminNowEventScene.hears(textData.eB.fee.faq.questionNumbers.thirdQuestion, setThirthIndexNowEvent, ('adminNowthirthEvent'))
adminNowEventScene.hears(`${textData.eB.backBtn} ${textData.menuText.backBtn}`, enter('adminEvents'))
adminNowEventScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminNowEventScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminNowEventScene 