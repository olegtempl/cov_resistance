const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const adminSettingsScene = new Scene('adminSettings');

/* 
    Добавить в настройки выбор языка для аккаунта с которого сидит человек (по Id записывается в бд админов поле language)
*/
adminSettingsScene.enter(
    (ctx) => ctx.reply(
        textData.cmdText.admin.settings.header, Markup
            .keyboard([
                [`${textData.eB.admBtn.settings.notiffication.header} ${textData.menuText.settings.notiffication.header}`],
                [`${textData.eB.backBtn} ${textData.menuText.backBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)

adminSettingsScene.hears(`${textData.eB.admBtn.settings.notiffication.header} ${textData.menuText.settings.notiffication.header}`, enter('adminNotiffication'))

adminSettingsScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminSettingsScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminSettingsScene