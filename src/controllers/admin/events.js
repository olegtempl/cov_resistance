const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const adminEventScene = new Scene('adminEvents');

adminEventScene.enter(
    (ctx) => ctx.reply(
        textData.cmdText.admin.events.new.header, Markup
            .keyboard([
                [`${textData.eB.admBtn.events.new.header} ${textData.menuText.admin.events.new.header}`,
                    `${textData.eB.admBtn.events.now.header} ${textData.menuText.events.now.header}`],
                [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)

adminEventScene.hears(`${textData.eB.admBtn.events.now.header} ${textData.menuText.events.now.header}`, enter('adminNowEvent'))
adminEventScene.hears(`${textData.eB.admBtn.events.new.header} ${textData.menuText.admin.events.new.header}`, enter('adminNewEvent'))

adminEventScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminEventScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminEventScene