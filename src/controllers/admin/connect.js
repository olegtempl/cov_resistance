const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../../utils/exportTextData'

const adminConnectScene = new Scene('adminConnect');

adminConnectScene.enter(
    (ctx) => ctx.reply(
        textData.cmdText.admin.connect.header, Markup
            .keyboard([
                [`${textData.eB.admBtn.connect.pull.header} ${textData.menuText.admin.connect.pull.header}`,
                    `${textData.eB.admBtn.connect.pinned.header} ${textData.menuText.connect.pinned.header}`],
                [`${textData.eB.admBtn.connect.post.header} ${textData.menuText.connect.post.header}`],
                [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
            ])
            .oneTime()
            .resize()
            .extra()
    )
)

adminConnectScene.hears(`${textData.eB.admBtn.connect.pull.header} ${textData.menuText.admin.connect.pull.header}`, enter('adminPull'))
adminConnectScene.hears(`${textData.eB.admBtn.connect.pinned.header} ${textData.menuText.connect.pinned.header}`, enter('adminPinned'))
adminConnectScene.hears(`${textData.eB.admBtn.connect.post.header} ${textData.menuText.connect.post.header}`, enter('adminPosr'))

adminConnectScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('adminMainMenu'))
adminConnectScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default adminConnectScene