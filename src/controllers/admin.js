const Scene = require('telegraf/scenes/base')

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import textData from '../utils/exportTextData'

const userOrAdminScene = new Scene('userOrAdmin');

userOrAdminScene.enter(
    (ctx) => ctx.reply(
    textData.cmdText.adminOrUser.header, Markup
            .keyboard([[`${textData.eB.admBtn.admin} ${textData.menuText.admin.admin}`,
                `${textData.eB.admBtn.user} ${textData.menuText.admin.user}`]
    ])
      .oneTime()
      .resize()
      .extra()
  )
)

userOrAdminScene.hears(`${textData.eB.adm.admin} ${textData.menuText.admin.admin}`, enter('adminMainMenu'))
userOrAdminScene.hears(`${textData.eB.cat.agile.header} ${textData.menuText.catalog.agile.header}`, enter('mainMenu')) 
userOrAdminScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))

export default userOrAdminScene