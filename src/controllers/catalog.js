const Scene = require('telegraf/scenes/base')

import Markup from 'telegraf/markup'
import textData from '../utils/exportTextData'
import Stage from 'telegraf/stage'
const {
  enter,
  leave
} = Stage

const catalogStartScene = new Scene('catalogStart')

const sendImg = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/aNPxwik'
  ).then(() => next())
}


catalogStartScene.enter(sendImg, (ctx) => ctx.reply(
    textData.cmdText.catalog.header, Markup
    .keyboard([
      [`${textData.eB.cat.eduMaterials.header} ${textData.menuText.catalog.eduMaterials.header}`,
        `${textData.eB.cat.document.header} ${textData.menuText.catalog.document.header}`
      ],
      [`${textData.eB.cat.liners.header} ${textData.menuText.catalog.liners.header}`,
        `${textData.eB.cat.countries.header} ${textData.menuText.catalog.countries.header}`
      ],
      [`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
  )

)
catalogStartScene.hears(`${textData.eB.cat.eduMaterials.header} ${textData.menuText.catalog.eduMaterials.header}`, enter('tractor'))
catalogStartScene.hears(`${textData.eB.cat.document.header} ${textData.menuText.catalog.document.header}`, (ctx) => {
  ctx.reply(
    'Данный функционал еще в разработке', Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})

catalogStartScene.hears(`${textData.eB.cat.liners.header} ${textData.menuText.catalog.liners.header}`, (ctx) => {
  ctx.reply(
    'Данный функционал еще в разработке', Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})

catalogStartScene.hears(`${textData.eB.cat.countries.header} ${textData.menuText.catalog.countries.header}`, enter('countries'))

catalogStartScene.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, enter('mainMenu'))

export default catalogStartScene