import Scene from 'telegraf/scenes/base'
import Extra from 'telegraf/extra'

const { enter, leave } = Stage
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import emodji from '../../locales/emodji'
import messagesText from '../../locales/messagesText'
import commandsText from '../../locales/commandsText'
import menuText from '../../locales/mainMenuText'

const {
    emodjiForText: eT,
    emodjiForButtons: eB,
    emodjiForNumbers: eN
} = emodji;


const { ruMenu: RU_MENU,
    byMenu: BY_MENU,
    enMenu: EN_MENU } = menuText;

const { ruMsgTxt: RU_MESSAGES,
    byMsgTxt: BY_MESSAGES,
    enMsgTxt: EN_MESSAGES } = messagesText;

const { ruCommandsText: RU_COMMANDS,
    byCommandsText: BY_COMMANBDS,
    enCommandsText: EN_COMMANBDS } = commandsText;

let
    menuButtonsText = RU_MENU,
    commandsTextLanguage = RU_COMMANDS,
    messagesTextLanguage = RU_MESSAGES; // default


/**
 * ToDo:
 *  * черпать имя и прочие данные по профилю из БД, а не из клиента телеги.
 *  * заносить данные в бд сразу после их обновления
 */
    
const settingsUserDataScene = new Scene('settingsUserData');

settingsUserDataScene.enter((ctx) => ctx.reply(commandsTextLanguage.settings.header, Markup
    .keyboard([
        [`${eB.set.userData.firstName} ${menuButtonsText.settings.userData.firstName}`,
        `${eB.set.userData.lastName} ${menuButtonsText.settings.userData.lastName}`],
        [`${eB.set.userData.phoneNumber} ${menuButtonsText.settings.userData.phoneNumber}`,
        `${eB.set.userData.town} ${menuButtonsText.settings.userData.town}`],
        [`${eB.set.userData.addres} ${menuButtonsText.settings.userData.addres}`,
        `${eB.set.userData.time} ${menuButtonsText.settings.userData.time}`],
        [`${eB.set.userData.email} ${menuButtonsText.settings.userData.email}`,
        `${eB.set.userData.town} ${menuButtonsText.settings.userData.town}`],

        [`${eB.backBtn} ${commandsTextLanguage.backBtn}`,
        `${eB.goMainMenuBtn} ${commandsTextLanguage.goMainMenuBtn}`]
    ])
    .oneTime()
    .resize()
    .extra()
))

settingsUserDataScene.hears(`${eB.set.userData.firstName} ${menuButtonsText.settings.userData.firstName}`, enter('settingsUserDataFirstName'))
// settingsUserDataScene.hears(`${eB.set.userData.lastName} ${menuButtonsText.settings.userData.lastName}`, enter('settingsFacts'))
// settingsUserDataScene.hears(`${eB.set.userData.phoneNumber} ${menuButtonsText.settings.userData.phoneNumber}`, enter('settingsFacts'))
// settingsUserDataScene.hears(`${eB.set.userData.addres} ${menuButtonsText.settings.userData.addres}`, enter('settingsFacts'))
// settingsUserDataScene.hears(`${eB.set.userData.time} ${menuButtonsText.settings.userData.time}`, enter('settingsFacts'))
// settingsUserDataScene.hears(`${eB.set.userData.email} ${menuButtonsText.settings.userData.email}`, enter('settingsFacts'))
// settingsUserDataScene.hears(`${eB.set.userData.town} ${menuButtonsText.settings.userData.town}`, enter('settingsFacts'))

settingsUserDataScene.hears(`${eB.backBtn} ${commandsTextLanguage.backBtn}`, enter('settings'))
settingsUserDataScene.hears(`${eB.goMainMenuBtn} ${commandsTextLanguage.goMainMenuBtn}`, enter('mainMenu'))
settingsUserDataScene.on('message', (ctx) => ctx.reply(messagesTextLanguage.ifUserSendMsgVicePressBtn))




export default settingsUserDataScene

// export default [
//     settingsUserDataScene,
//     clientsScene, 
//     // factsScene,
//     // projectsScene,
//     // servicesScene,
//     // contactsScene,
//     // ourAdvantagesScene,
//     // teamScene,
//     // partnersScene,
//     // qualityGuaranteeScene,
//     // whoWeAreScene
// ]