import Telegraf from 'telegraf'
// import cl from 'node-cl-log'
import session from 'telegraf/session'
import Stage from 'telegraf/stage'
import Markup from 'telegraf/markup'
import Scene from 'telegraf/scenes/base'
import getFortuneNote from './utils/fortuneGenerator'

// Данный функционал в разработке. В ближайшее время он будет доступен. Если вас заинтересовало что-нибудь из каталога, связитесь с командой проекта через кнопку обратная связи или команду /contacts.


const cron = require('node-cron');
// const alarmCron = (numberTime, ctx) => {
//   cron.schedule(`${numberTime[1]} ${numberTime[0]} * * *`, () => {
//     // console.log(getFortuneNote())
//     ctx.telegram.sendMessage('-1001263977856',
//       `🏁 Через  5️⃣ минут начинается стэндап. А пока вот вам цитата дня:

//       ${getFortuneNote()}

// `);
//   });
// };

// import logger from './utils/logger'
import textData from './utils/exportTextData'
import exportScenes from './utils/exportScenes'
import mainMenuScene from './controllers/mainMenu'
import instructionScene from './controllers/instruction'

const chooseEnLang = (ctx, next) => {
  // textData.setLang(bot.context.languages.en)
  // ctx.reply(`${textData.eB.langSet.en} ${textData.cmdText.langSet.en}`).then(() => next());
  ctx.reply(`${textData.eB.langSet.en} ${textData.cmdText.langSet.en}`).then(() => next());
};
const chooseBeLang = (ctx, next) => {
  // textData.setLang(bot.context.languages.be)
  // ctx.reply(`${textData.eB.langSet.be} ${textData.cmdText.langSet.be}`).then(() => next());
  ctx.reply(`${textData.eB.langSet.be} ${textData.cmdText.langSet.be}`).then(() => next());
};
const chooseRuLang = (ctx, next) => {
  textData.setLang(bot.context.languages.ru)
  ctx.reply(`${textData.eB.langSet.ru} ${textData.cmdText.langSet.ru}`).then(() => next());
};




const chooseLangScene = new Scene('chooseLang')

chooseLangScene.enter((ctx) => {
  // logger(ctx)

  // ${textData.eB.langSet.en} ${textData.menuText.langSet.headerEn} ${textData.eB.langSet.en}
  // ${textData.eB.langSet.be} ${textData.menuText.langSet.headerBe} ${textData.eB.langSet.be}
  // ${textData.eB.langSet.ru} ${textData.menuText.langSet.headerRu} ${textData.eB.langSet.ru}
  // ctx.reply(
  //   `вместо текст изображение и кнопки`, Markup.keyboard([
  //     [
  //       `${textData.eB.langSet.en} ${textData.menuText.langSet.en}`,
  //       `${textData.eB.langSet.ru} ${textData.menuText.langSet.ru}`
  //     ],
  //     [
  //       `${textData.eB.langSet.be} ${textData.menuText.langSet.be}`

  //     ]
  //   ])
  //   .oneTime()
  //   .resize()
  //   .extra()
  // )
  ctx.replyWithPhoto(
    'https://imgur.com/Y0ZJ0mI', Markup.keyboard([
      [
        `${textData.eB.langSet.en} ${textData.menuText.langSet.en}`,
        `${textData.eB.langSet.ru} ${textData.menuText.langSet.ru}`
      ],
      [
        `${textData.eB.langSet.be} ${textData.menuText.langSet.be}`

      ]
    ])
    .oneTime()
    .resize()
    .extra()
  )
})


let stageScenesArray = []
stageScenesArray = stageScenesArray.concat(mainMenuScene, instructionScene, exportScenes)
stageScenesArray.push(chooseLangScene)

const stage = new Stage(stageScenesArray)

chooseLangScene.hears(`${textData.eB.langSet.en} ${textData.menuText.langSet.en}`, chooseEnLang, (ctx) => {
  // ctx.scene.enter('mainMenu')
  ctx.reply(`
  Greetings 👐

  🛠 I am currently working in open beta mode and development is still in progress. At the moment, the bot version is available only in Russian language.

  The following features will be available soon:
    ⚙️

  ℹ️ By the way, you may be interested in this community 👥 https://t.me/DarkDev_brand or our Instagram 📷 https://www.instagram.com/darkdev_brand.
  🔻 If something is broken, write to us in the project community, or to one of us 👨‍💻 https://t.me/oleg_DarkDev_brand, we will try to fix it.

  Best regards from DarkDev brand 💙`, Markup
    .keyboard([
      `${textData.eB.langSet.ru} ${textData.menuText.langSet.ru}`
    ])
    .oneTime()
    .resize()
    .extra())
})
chooseLangScene.hears(`${textData.eB.langSet.be} ${textData.menuText.langSet.be}`, chooseBeLang, (ctx) => {
  // ctx.scene.enter('mainMenu')
  ctx.reply(`
  Вітаю 👐

  🛠 Зараз я працую ў рэжыме адкрытага бэта-тэсту і распрацоўка ўсё яшчэ ў працэсе. На дадзены момант даступна версія бота толькі на рускай мове.

  Хутка будзе даступны наступны функцыянал:
    ⚙️


  ℹ️ Дарэчы магчыма вам будзе цікава дадзеная суполка 👥 https://t.me/DarkDev_brand  ці наш инстаграмм 📷 https://www.instagram.com/darkdev_brand.
  🔻 Калі нешта зламалася, напішы нам у суполку праекта, ці аднаму з нас 👨‍💻 https://t.me/oleg_DarkDev_brand, мы паспрабуем гэта паправіць.

  З найлепшымі пажаданнямі ад праекта DarkDev brand 💙`, Markup
    .keyboard([
      `${textData.eB.langSet.ru} ${textData.menuText.langSet.ru}`
    ])
    .oneTime()
    .resize()
    .extra())
})
chooseLangScene.hears(`${textData.eB.langSet.ru} ${textData.menuText.langSet.ru}`, chooseRuLang, (ctx) => {
  ctx.scene.enter('instruction')
})

// chooseLangScene.on('message', (ctx) => ctx.reply(textData.msgText.ifUserSendMsgVicePressBtn))


const BOT_TOKEN = "..";
const bot = new Telegraf(BOT_TOKEN);

bot.use(session())
bot.use(stage.middleware())

bot.context.languages = {
  en: 'en',
  be: 'be',
  ru: 'ru',
}

bot.context.savedEasyScene = '0';

bot.use((ctx, next) => {
  ctx.state.savedEasyScene = '0'
  return next()
})


bot.command('start', (ctx) => {
  ctx.scene.enter('chooseLang')
})


bot.command('test', (ctx) => {
  // ctx.scene.enter('catalogStart')
  ctx.scene.enter('gameModeEasyStartDays')
  // console.log('store')
})


const sendImgDarkDev = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/I2r1fVg'
  ).then(() => next())
}


bot.command('darkdev', sendImgDarkDev, (ctx) => {
  ctx.reply(
    `Цель проекта DarkDev brand - делать доступными обучающие игры, инструменты, ПО для самоорганизующихся команд и организаций с плоской структурой.

    ℹ️ На данный момент сфокусированы на создании обучающих игр, геймификации в обучении и проведении воркшопов. Все игры и программы воркшопов мы стараемся переводить на 3 языка: русский, беларуский, английский.

    Наш канал в телеграме: 👥 https://t.me/DarkDev_brand
    Наш профиль в инстаграме: 📷 https://www.instagram.com/darkdev_brand

    Сейчас проект активно ищет инвесторов и поддержку 💰

    С наилучшими пожеланиями от проекта DarkDev brand 💙.
    `, Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})



const sendImgSettings = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/XI9brII'
  ).then(() => next())
}

bot.command('settings', sendImgSettings, (ctx) => {
  ctx.reply(
    'Данный функционал еще в разработке', Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})



const sendImgLibrary = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/aNPxwik'
  ).then(() => next())
}

bot.command('library', sendImgLibrary, (ctx) => {
  ctx.scene.enter('catalogStart')
})



const sendImgHelp = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/hnO38CY'
  ).then(() => next())
}


bot.command('help', sendImgHelp, (ctx) => {
  ctx.reply(
    `Данный функционал еще в разработке, но на данный момент доступны следующие команды:

    /game - перейти в игровой режим
    /library - каталог обучающих материалов
    /tractor - социальный проект "Беларуский трактор"
    /help - справочное окно
    /settings - настройки личного кабинета
    /darkdev - о проекте DarkDev brand
    /faq - показать F.A.Q`, Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})


const sendImgTractor = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/Y1OuA7d'
  ).then(() => next())
}


bot.command('tractor', sendImgTractor, (ctx) => {
  ctx.scene.enter('tractor')
})




const sendImgFaq = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/ngG2c7a'
  ).then(() => next())
}

bot.command('faq', sendImgFaq, (ctx) => {
  ctx.reply(
    'Данный функционал еще в разработке', Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})



const sendImgGame = (ctx, next) => {
  ctx.replyWithPhoto(
    'https://imgur.com/60JbsSb'
  ).then(() => next())
}


bot.command('game', sendImgGame, (ctx) => {
  ctx.scene.enter('game')
})





bot.command('langen', chooseEnLang, (ctx) => {
  ctx.reply(
    `  Greetings 👐

    🛠 I am currently working in open beta mode and development is still in progress. At the moment, the bot version is available only in Russian language.

    The following features will be available soon:
      ⚙️

    ℹ️ By the way, you may be interested in this community 👥 https://t.me/DarkDev_brand or our Instagram 📷 https://www.instagram.com/darkdev_brand.
    🔻 If something is broken, write to us in the project community, or to one of us 👨‍💻 https://t.me/oleg_DarkDev_brand, we will try to fix it.

    Best regards from DarkDev brand 💙`, Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})


bot.command('langby', chooseBeLang, (ctx) => {
  ctx.reply(
    ` Вітаю 👐

    🛠 Зараз я працую ў рэжыме адкрытага бэта-тэсту і распрацоўка ўсё яшчэ ў працэсе. На дадзены момант даступна версія бота толькі на рускай мове.

    Хутка будзе даступны наступны функцыянал:
      ⚙️


    ℹ️ Дарэчы магчыма вам будзе цікава дадзеная суполка 👥 https://t.me/DarkDev_brand  ці наш инстаграмм 📷 https://www.instagram.com/darkdev_brand.
    🔻 Калі нешта зламалася, напішы нам у суполку праекта, ці аднаму з нас 👨‍💻 https://t.me/oleg_DarkDev_brand, мы паспрабуем гэта паправіць.

    З найлепшымі пажаданнямі ад праекта DarkDev brand 💙`, Markup
    .keyboard([
      `${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`
    ])
  )
})


bot.hears(`${textData.eB.goMainMenuBtn} ${textData.menuText.goMainMenuBtn}`, (ctx) => {
  ctx.scene.enter('mainMenu')
})


bot.launch()