import catalogStartScene from "../../controllers/catalog"

// Country scenes
import countriesScene from "../../controllers/catalog/countries"
import germanyScene from "../../controllers/catalog/countries/germany"

// Tractor scenes
import tractorScene from "../../controllers/catalog/tractor"
import tractorEducationScene from "../../controllers/catalog/tractor/education"
import tractorEatScene from "../../controllers/catalog/tractor/eat"
import tractorBooksScene from "../../controllers/catalog/tractor/books"
import tractorBarsScene from "../../controllers/catalog/tractor/bars"
import tractorArtScene from "../../controllers/catalog/tractor/art"
import tractorKinoScene from "../../controllers/catalog/tractor/kino"
import tractorParentsScene from "../../controllers/catalog/tractor/parents"
import tractorPodcastsScene from "../../controllers/catalog/tractor/podcasts"
import tractorSportScene from "../../controllers/catalog/tractor/sport"
import tractorTravelsScene from "../../controllers/catalog/tractor/travels"
import tractorMonitoringScene from "../../controllers/catalog/tractor/monitoring"


export default [
  catalogStartScene,

  // Tractor scenes
  tractorScene,
  tractorEducationScene,
  tractorEatScene,
  tractorBooksScene,
  tractorBarsScene,
  tractorArtScene,
  tractorKinoScene,
  tractorParentsScene,
  tractorPodcastsScene,
  tractorSportScene,
  tractorTravelsScene,
  tractorMonitoringScene,

  // Country scenes
  countriesScene,
  germanyScene,

]