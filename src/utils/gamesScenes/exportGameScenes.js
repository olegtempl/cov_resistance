import gameStartScene from "../../controllers/game"
import gameModeScene from "../../controllers/game/gameMode"
import gameSavedScene from "../../controllers/game/saved"

// Hard mode scenes
import gameModeHardScenes from "../../controllers/game/gameMode/hard"


// Easy mode scenes
import gameModeEasyScenes from "../../controllers/game/gameMode/easy"
import gameModeEasyStartDaysScene from "../../controllers/game/gameMode/easy/startDays"
import easyInstructionScene from "../../controllers/game/gameMode/easy/instruction"


export default [
  gameStartScene,
  gameModeScene,
  gameSavedScene,

  // Easy mode scenes
  gameModeEasyScenes,
  easyInstructionScene,
  gameModeEasyStartDaysScene,


  // Hard mode scenes
  gameModeHardScenes,



]