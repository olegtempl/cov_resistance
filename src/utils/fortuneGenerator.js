import fortunes from '../data/fortunes'


function getFortuneNote() {
  let fortunesListLength = fortunes.length;

  let randomNumber = Math.floor(Math.random() * (fortunesListLength - 0 + 1)) + 0;
  return fortunes[randomNumber]
}


export default getFortuneNote